unzip -o ../data/DemoMine_820.zip -d .
export PASS='YourStrong!Password'
sudo docker run --rm --name db_test_geotic -e "ACCEPT_EULA=y" -e "MSSQL_SA_PASSWORD=$PASS" -p 1433:1433 -d -v sql1data:/var/opt/mssql mcr.microsoft.com/mssql/server:2022-latest
sudo docker cp DemoMine_820.bak db_test_geotic:/var/opt/mssql/backup
sudo docker exec -it db_test_geotic /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P $PASS -Q "RESTORE FILELISTONLY FROM DISK = "/var/opt/mssql/backup/DemoMine_820.bak""
sudo docker exec -it db_test_geotic /opt/mssql-tools/bin/sqlcmd -S localhost -U SA -P $PASS -Q "RESTORE DATABASE GeoticDb FROM DISK= "/var/opt/mssql/backup/DemoMine_820.bak" WITH MOVE "DemoMine" TO "/var/opt/mssql/data/DemoMine.mdf", MOVE "DemoMine_Log" TO "/var/opt/mssql/data/DemoMine.ldf""
