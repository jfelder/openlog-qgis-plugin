import pytest
from PyQt5.QtWidgets import QMessageBox

from openlog.datamodel.connection.openlog_connection import Connection
from openlog.gui.connection.connection_widget import ConnectionWidget
from openlog.gui.connection.postgis_connection_widget import PostgisConnectionWidget


@pytest.fixture()
def connection_widget(qtbot):
    widget = PostgisConnectionWidget()
    qtbot.addWidget(widget)
    yield widget


@pytest.fixture()
def connection_widget_filled(connection_widget, connection: Connection):
    connection_widget.set_connection_model(connection)
    yield connection_widget


def test_connection_widget_invalid_connection(connection_widget):
    with pytest.raises(ConnectionWidget.InvalidConnection):
        connection_widget.get_connection()


def test_connection_widget_test_connection(connection_widget, mocker):
    msg_box_mock = mocker.patch.object(QMessageBox, 'warning', return_value=QMessageBox.Ok)
    connection_widget.test_connection()
    msg_box_mock.assert_called_once()


def test_connection_widget_valid_connection(connection_widget_filled):
    assert connection_widget_filled.get_connection()
