def test_spatialite_connection(plugin_spatialite_create):
    # Check that surveying action is not enabled
    assert not plugin_spatialite_create.selected_colar_desurv_action.isEnabled()

    # Check that no feature are available in collar layer
    assert (
        plugin_spatialite_create.openlog_connection.get_layers_iface()
        .get_collar_layer()
        .featureCount()
        == 0
    )

    # Check that no trace are available
    assert (
        plugin_spatialite_create.openlog_connection.get_layers_iface()
        .get_collar_trace_layer()
        .featureCount()
        == 0
    )
