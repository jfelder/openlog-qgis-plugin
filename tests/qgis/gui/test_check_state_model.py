import pytest

from PyQt5.QtCore import Qt, QObject
from openlog.gui.check_state_model import CheckStateModel


@pytest.fixture()
def parent_obj():
    return QObject()


@pytest.fixture()
def model(parent_obj):
    res = CheckStateModel(parent_obj)

    # Add row in root
    row = res.rowCount()
    res.insertRow(row)
    res.insertColumn(0)
    root = res.index(row, 0)
    res.setData(root, "root")
    res.setData(root, Qt.Unchecked, Qt.CheckStateRole)

    # Insert column in index for child display
    res.insertColumn(0, root)

    # Two childs
    res.insertRow(0, root)
    child = root.child(0, 0)
    res.setData(child, "child1")
    res.setData(child, Qt.Unchecked, Qt.CheckStateRole)

    res.insertRow(1, root)
    child = root.child(1, 0)
    res.setData(child, "child2")
    res.setData(child, Qt.Unchecked, Qt.CheckStateRole)

    return res


def test_model_creation(model):
    parent = model.index(0, 0)
    assert model.rowCount(parent) == 2
    for i in range(0, model.rowCount(parent)):
        assert model.data(parent.child(i, 0), Qt.CheckStateRole) == Qt.Unchecked


def test_children_check(model):
    parent = model.index(0, 0)
    model.setData(parent, Qt.Checked, Qt.CheckStateRole)
    for i in range(0, model.rowCount(parent)):
        assert model.data(parent.child(i, 0), Qt.CheckStateRole) == Qt.Checked


def test_parent_check(model):
    parent = model.index(0, 0)
    child = model.index(0, 0, parent)
    model.setData(child, Qt.Checked, Qt.CheckStateRole)

    assert model.data(parent, Qt.CheckStateRole) == Qt.PartiallyChecked

    child = model.index(1, 0, parent)
    model.setData(child, Qt.Checked, Qt.CheckStateRole)

    assert model.data(parent, Qt.CheckStateRole) == Qt.Checked
