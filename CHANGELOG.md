# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

<!--

Unreleased

## version_tag - YYYY-DD-mm

### Added

### Changed

### Removed

-->
## 0.9.2 - 2023-03-xx
Bugfixes version

Fixes:

- Invalid conversion in BDGeo for temperature unit #185

## 0.9.1 - 2023-02-21
There is a major issue when no unit is defined. User can't access downhole data visualization configuration panel.

Fixes:

- Error when no units defined #181

## 0.9.0 - 2023-02-13
Ninth alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

New features:

- Custom projected CRS definition #103
- Support for SI units in xplordb and OpenLog #158

Fixes:

- Won't create database if date is blank #171

## 0.8.2 - 2023-02-06
Some users reported issue with file import on Windows. For better compatibility, you can now define encoding use for import.
An automatic detection of encoding is done with chardet.

Feature :

- define file encoding !206

## 0.8.1 - 2023-01-20
Fix version for the 0.8.0.

Fixes :

- data import impossible in Windows version #170

## 0.8.0 - 2023-01-12

Eight alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

This version focus on BDGeo support. Documentation and terminology was updated.

You can also now move your downhole data column in a specific widget or display all column in a stacked plot.

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

New features:

- Update terminology #157
- Split graph title over 3 lines #153
- Define style for each family of BDGeo station #147
- Correct display of BDGeo assay with Chimie category #146
- Update read of available assays to add mesure category if duplicated name #145
- Distinguish nan from null entries in importer #133
- Add option to switch from line/point chart to bar chart for discrete numerical assays #122
- Add option to display assay columns in separated widget !183 !197 !196
- Documentation update !195
- Random color for assay pen !190
- Support for uncertainty display in BDGeo !189
- Access to plot options to be restricted #75

Fixes:

- Stacked entry always present in symbology options !168

For developers:

- documentation for code !182
- refactoring use of sqlalchemy for assay read and import !186

## 0.7.0 - 2022-11-07

Seventh alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

This is mainly and refactoring (ui and code) and bugfixes version.

Translation support is also introduced : Bienvenue à nos utilisateurs français 🇫🇷 !

If you want to add support for a new langage, please add an issue in GitLab. We will be pleased to help you !

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.


New features:

- get collar elevation from DTM when importing collars #114
- apply a semi-random colour scheme to categorical extended assay #102
- add options to define time format in assay import #86
- add option to switch from bar to line chart for extended numerical assays #83
- UI decluterring: Move categorical data symbology control to general symbology table #110
- UI decluterring: merge symbology application button into a single drop down menu #111
- add some icons and refactor of ui (Thanks @sbeorchia !) !151
- first version of French translation !160
- limit visible assay depending on BDGeo station family #127
- add explicit option to select no DTM for collar creation #128
- Add option to delete lines in Collar creation wizard #129
- Automatic assay column name from selected import column !174
- Define specific column name for assay uncertainty !170
- Define default expand state for assay configuration !175

Fixes:

- allow use of None value in assay !159
- remove limits on index start for collar creation #124
- allow to remove definition of uncertainty and not mandatory columns #123
- import of data with null values #105
- exception raised in certain conditions when updating assay column configuration #73
- UI decluterring: merge symbology application button into a single drop down menu #
- crash in python 3.10 for categorical extended assay display !152
- Multi-column assay import wizard backtracking issue #130
- Assay importer crash on depth extended categorical data #131
- Assay importer error on duplicate field name for data and uncertainty fields #132
- Collar import fails to retrieve DTM height values #141

For developers:

- refactor OpenLogConnection #66
- refactor AssayWidget !156

## 0.6.1 - 2022-10-14

Fixes:

- Invalid assay display in data was not inserted in x value order #117
- Plugin package release link created by CI is not valid #118
- Define minimum version for some python package and display import error at plugin launch #119

## 0.6.0 - 2022-09-29

Sixth alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

New features:

- depth and time assay graph ordering option #97
- add inspector line to display value of assay #99
- add option to display marker for discrete assay #98
- add boxplot representation for uncertainty data #91
- add colorramp option for pen of discrete assay #81
- add categories management in assay and lithology import #64
- get collar elevation from DTM #43
- add feature for bulk collar creation #23

Fixes:

- merge available symbology when load symbology .json #108
- introduce minimum size for assay plot for correct display #107
- insertion of lithology in Xplordb !137
- data importation : remove rows with mandatory value not defined !141

## 0.5.1 - 2022-08-29

Bug fixes version for 0.5.0.

### Fixes

- error display when defining desurveying even if data is valid

## 0.5.0 - 2022-08-23

Fifth alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

This version introduces support for stacked assay creation, assay units and uncertainty definition.

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

### New features

- define assay column units, type and uncertainty
- create stacked assay
- display assay uncertainty
- scroll on assay graph with CTRL+wheel
- add length numbering option on collar creation
- ask user for assay configuration save if categorical configuration changed
- refactor tests for Spatialite and Xplordb connection

### Fixes

- catch data import exception

### Upcoming features

- new types for generic assay import
- bulk collar creation
- CSV file exporter

## 0.4.0 - 2022-07-06

Fourth alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

This version introduces experimental support for time assay visualization and BDGeo assay visualization.

User can now save current OpenLog connection in QGIS project : for connection with authentication, a connection dialog is displayed when opening QGIS project.

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

### New features

- save current OpenLog connection in QGIS project : for connection with authentication, a connection dialog is displayed when opening QGIS project
- access assay visualization widget from QGIS canvas custom menu
- propagate assay visualization configuration
- simple progress dialog when importing collar assays
- connection name indicated in layers and QGIS main window title
- (experimental) support for BDGeo assay display
- (experimental) support for time assay display

### Fixes

- center vertically categorical assay text

### Upcoming features

- new types for generic assay import
- bulk collar creation
- CSV file exporter

## 0.3.0 - 2022-06-03

Third alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

This version introduces stratigraphy log viewer with lithology as categorical extended assay and user can define any column for assay definition.

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

### New features

- (experimental) stratigraphy log viewer
- (experimental) support for extended assay display (categorical and numerical)
- assay can have several column for data
- ask for connections creation after xplordb database creation
- ask for connection save after modification
- add some configuration for assay line display
- display recent spatialite database

### Fixes

- automatic desurveying after survey definition on spatialite
- some import where failing when hole_id could be imported as float

### Upcoming features

- new types for generic assay import
- bulk collar creation
- CSV file exporter

## 0.2.0 - 2022-05-06

Second alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🎂

This version introduces the notion of assay and some graphical interfaces to create / import / visualize them.

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

### New features

- CSV import of generic assay files into spatialite and xplordb databases
- (experimental) visualization of assay in depth domain
- (experimental) connection to Acquire database for collar and survey display

### Upcoming features

- update of generic assay import to define several columns for assay definition
- stratigraphy log viewer
- new types for generic assay import
- bulk collar creation
- CSV file exporter

## 0.1.0 - 2022-04-01

First alpha version of the [OpenLog](https://oslandia.com/en/openlog/) QGIS plugin 🍾

This version features a variety of brand new experimental features as well as production-ready elements.

Although most of the development efforts have been focussed on the database-I/O-architecture trifecta, basic 3D visualization for surveys is already available. It is already possible to display desurveyed drillhole projected traces on the map.

[Feedback](https://gitlab.com/geolandia/openlog/openlog-qgis-plugin/-/issues/new) is welcome through on Gitlab, make sure to follow our reporting template and check for potential duplicates beforehand.

### New features

- creation / connection to spatialite database for collar and survey storage
- creation of xplordb database on a postgresql server
- (experimental) connection to Geotic database for collar and survey display
- (experimental) connection to BDGeo database for collar display
- CSV import of collar, survey, and lithology files into spatialite and xplordb databases
- automated lithological data gap/overlap discrepancy resolution on import
- creation of collar from QGIS map
- survey table content creation for newly created collars
- 2D/3D desurveying (manual in spatialite, automatic in xplordb)

### Upcoming features

- CSV import of generic assay files into spatialite and xplordb databases
- connection to Acquire database for collar and survey display
