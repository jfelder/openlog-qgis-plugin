from dataclasses import dataclass, field
from typing import List

import pyqtgraph as pg

from openlog.datamodel.assay.generic_assay import AssaySeriesType
from openlog.gui.assay_visualization.assay_visualization_config import (
    AssayColumnVisualizationConfig,
)


@dataclass
class StackedConfiguration:
    """
    StackedConfiguration
    Contains list of AssayColumnVisualizationConfig
    Can also access plot and plot item if configuration created from visualization widget

    """

    is_visible: bool = True
    plot: pg.PlotWidget = None
    name: str = ""
    config_list: List[AssayColumnVisualizationConfig] = field(default_factory=list)

    def set_plot(self, plot: pg.PlotWidget) -> None:
        """
        Define plot containing current stacked data

        Args:
            plot: pg.PlotWidget
        """
        self.plot = plot

    @staticmethod
    def invalid_series_type() -> [AssaySeriesType]:
        return [AssaySeriesType.CATEGORICAL, AssaySeriesType.NOMINAL]

    def is_valid(self) -> bool:
        """
        Check if StackedConfiguration is valid :
        - only one unit used
        - only one series type used
        - only valid series type used

        Returns: True if StackedConfiguration is valid, False otherwise

        """
        valid = True
        units = set([c.column.unit for c in self.config_list])
        # Get all units
        valid = valid and len(units) == 1

        # Get all series_type
        series_type = set([c.column.series_type for c in self.config_list])
        valid = valid and len(series_type) == 1

        for invalid_series_type in self.invalid_series_type():
            valid = valid and invalid_series_type not in series_type
        return valid


@dataclass
class StackedConfigurationList:
    def __init__(self):
        """
        Container of StackedConfiguration.
        List can be accessed with list attributes.

        Define several method for list modification and query

        """
        self.list = []

    def remove_config(self, config: StackedConfiguration) -> None:
        """
        Remove configuration from list
        If plot is associated to configuration, plot is also deleted

        Args:
            config: (StackedConfiguration) configuration
        """

        if config.plot is not None:
            # Update visibility even if plot is deleted later
            # so we can count number of visible plot if main thread as not process event
            config.plot.setVisible(False)
            config.plot.deleteLater()
        self.list.remove(config)
