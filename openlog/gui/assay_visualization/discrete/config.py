import secrets
from typing import Any

import pyqtgraph as pg
import pyqtgraph.parametertree as ptree
import pyqtgraph.parametertree.parameterTypes as pTypes
from qgis.PyQt.QtGui import QColor

from openlog.core import pint_utilities
from openlog.datamodel.assay.generic_assay import AssayDomainType, GenericAssay
from openlog.datamodel.assay.uncertainty import UncertaintyType
from openlog.gui.assay_visualization.assay_visualization_config import (
    AssayColumnVisualizationConfig,
)
from openlog.gui.assay_visualization.plot_item_factory import AssayPlotItemFactory


class DiscreteAssayColumnVisualizationConfig(AssayColumnVisualizationConfig):
    def __init__(self, column: str, extended_config: Any = None):
        """
        Store visualization configuration for a discrete assay column.
        Can also access plot item if configuration created from visualization widget

        Configuration supported :
        - visibility_param (bool) : assay column visibility (default : True)
        - pen_params (QPen) : assay column pen (default : black)
        - uncertainty_visibility_param (bool) : uncertainty visibility (default : True) (available in uncertainty defined)

        Args:
            column: (str) assay column name
        """
        super().__init__(column)
        self.as_extended = False
        self.extended_config = extended_config

        self.btn = ptree.Parameter.create(name=self.tr("Bar chart"), type="action")
        self.btn.sigActivated.connect(self._switch_to_extended)

        color = "#" + "".join([secrets.choice("0123456789ABCDEF") for _ in range(6)])
        pen = self.pen_params.pen
        pen.setBrush(QColor(color))

        if self.column.uncertainty.get_uncertainty_type() != UncertaintyType.UNDEFINED:
            self.uncertainty_visibility_param = ptree.Parameter.create(
                name=self.tr("Uncertainty"), type="bool", value=True, default=True
            )
            self.uncertainty_visibility_param.sigValueChanged.connect(
                self._uncertainty_visibility_changed
            )
        else:
            self.uncertainty_visibility_param = None

        self.symbol_group = ptree.Parameter.create(name=self.tr("Point"), type="group")
        self.symbol_group.setOpts(expanded=False)
        self.symbol_parameter = pTypes.ListParameter(
            name=self.tr("Point"),
            type="str",
            value="",
            default="",
            limits=["", "o", "s", "x", "d"],
        )
        self.symbol_group.addChild(self.symbol_parameter)
        self.symbol_size_parameter = pTypes.SimpleParameter(
            name=self.tr("Size"), type="int", value=5, default=5, min=1, max=20
        )
        self.symbol_group.addChild(self.symbol_size_parameter)
        self.symbol_color_parameter = pTypes.ColorParameter(
            name=self.tr("Color"), value="blue", default="blue"
        )
        self.symbol_group.addChild(self.symbol_color_parameter)

        self.color_ramp_group = ptree.Parameter.create(
            name=self.tr("Color ramp"), type="group"
        )
        self.color_ramp_group.setOpts(expanded=False)
        list_of_maps = pg.colormap.listMaps()
        # list_of_maps += pg.colormap.listMaps('matplotlib')
        # list_of_maps += pg.colormap.listMaps('colorcet')
        list_of_maps = sorted(list_of_maps, key=lambda x: x.swapcase())

        self.color_ramp_name_param = pTypes.ListParameter(
            name=self.tr("Name"),
            type="str",
            value="PAL-relaxed_bright",
            default="PAL-relaxed_bright",
            limits=list_of_maps,
        )
        self.color_ramp_group.addChild(self.color_ramp_name_param)

        self.color_ramp_parameter = pTypes.ColorMapParameter(name=self.tr("Scale"))
        self.color_ramp_group.addChild(self.color_ramp_parameter)
        self.min_color_ramp_param = pTypes.SimpleParameter(
            name=self.tr("Min"), type="float", value="0.0"
        )
        self.color_ramp_group.addChild(self.min_color_ramp_param)
        self.max_color_ramp_param = pTypes.SimpleParameter(
            name=self.tr("Max"), type="float", value="1.0"
        )
        self.color_ramp_group.addChild(self.max_color_ramp_param)
        self.pen_color_ramp_use_param = ptree.Parameter.create(
            name=self.tr("Enable"),
            type="bool",
            value=False,
            default=False,
        )
        self.color_ramp_group.addChild(self.pen_color_ramp_use_param)

        self.pen_color_ramp_use_param.sigValueChanged.connect(
            self._update_plot_item_color_ramp
        )
        self.color_ramp_name_param.sigValueChanged.connect(
            self._update_color_ramp_from_name
        )
        self.min_color_ramp_param.sigValueChanged.connect(
            self._update_plot_item_color_ramp
        )
        self.max_color_ramp_param.sigValueChanged.connect(
            self._update_plot_item_color_ramp
        )
        self.color_ramp_parameter.sigValueChanged.connect(
            self._update_plot_item_color_ramp
        )

        self._uncertainty_plot_items = []

        # Connect to extended config unit param change to update current unit
        if self.extended_config:
            self.extended_config.unit_parameter.sigValueChanged.connect(
                self._extended_unit_updated
            )

    def set_assay(self, assay: GenericAssay) -> None:
        super().set_assay(assay)
        if self.extended_config:
            self.extended_config.set_assay(assay)

    def _visibility_changed(self) -> None:
        """
        Update plot visibility when parameter is changed

        """
        super()._visibility_changed()
        for plot_item in self._uncertainty_plot_items:
            visible = (
                self.uncertainty_visibility_param.value()
                and self.visibility_param.value()
            )
            plot_item.setVisible(visible)

    def _pen_updated(self) -> None:
        """
        Update plot pen when parameter is changed : use color ramp or pen value

        """
        if self.plot_item:
            self._update_plot_item_color_ramp()

    def stack_widget(self, plot_stacked: pg.PlotWidget) -> None:
        """
        Add current plot item to a new plot widget and remove them from current plot widget

        Args:
            plot_stacked (pg.PlotWidget): plot widget where item are added
        """
        super().stack_widget(plot_stacked)
        if self.plot_widget and not self.as_extended:
            for item in self._uncertainty_plot_items:
                self.plot_widget.removeItem(item)
                plot_stacked.addItem(item)

    def unstack_widget(self, plot_stacked: pg.PlotWidget) -> None:
        """
        Remove current plot item from a stacked plot widget and add them to current plot widget

        Args:
            plot_stacked (pg.PlotWidget): plot widget where items are available
        """
        super().unstack_widget(plot_stacked)
        if self.plot_widget and not self.as_extended:
            for item in self._uncertainty_plot_items:
                plot_stacked.removeItem(item)
                self.plot_widget.addItem(item)

    def set_uncertainty_plot_items(self, plot_items: [pg.PlotDataItem]) -> None:
        """
        Define plot items containing current assay data uncertainty

        Args:
            plot_items: [pg.PlotDataItem]
        """
        self._uncertainty_plot_items = plot_items

    def _uncertainty_visibility_changed(self) -> None:
        """
        Update plot uncertainty item visibility when parameter is changed

        """
        visible = (
            self.uncertainty_visibility_param.value() and self.visibility_param.value()
        )
        for plot_item in self._uncertainty_plot_items:
            plot_item.setVisible(visible)

    def add_children_to_root_param(self, params: ptree.Parameter):
        if self.extended_config:
            if self.as_extended:
                self.btn.setName(self.tr("Line chart"))
                params.addChild(self.btn)
                self.extended_config.add_children_to_root_param(params)
            else:
                self.btn.setName(self.tr("Bar chart"))
                params.addChild(self.btn)
                self._add_discrete_children_to_root_param(params)
        else:
            self._add_discrete_children_to_root_param(params)

    def _add_discrete_children_to_root_param(self, params: ptree.Parameter) -> None:
        super().add_children_to_root_param(params)
        if self.uncertainty_visibility_param:
            # Need to recreate parameter or a exception is raised by pyqtgraph because checkbox widget can be deleted when cleaning root param
            self.uncertainty_visibility_param = ptree.Parameter.create(
                name=self.tr("Uncertainty"),
                type="bool",
                value=self.uncertainty_visibility_param.value(),
                default=True,
            )
            self.uncertainty_visibility_param.sigValueChanged.connect(
                self._uncertainty_visibility_changed
            )
            params.addChild(self.uncertainty_visibility_param)
        params.addChild(self.symbol_group)
        params.addChild(self.color_ramp_group)

    def set_plot_item(self, plot_item: pg.PlotDataItem) -> None:
        """
        Define plot item containing current assay data

        Args:
            plot_item: pg.PlotDataItem
        """
        super().set_plot_item(plot_item)
        if self.plot_item:
            # Define current parameters
            if self.symbol_parameter.value():
                self.plot_item.setSymbol(self.symbol_parameter.value())
            else:
                self.plot_item.setSymbol(None)
            self.plot_item.setSymbolSize(self.symbol_size_parameter.value())
            self.plot_item.setSymbolBrush(self.symbol_color_parameter.value())

            if self.plot_item.xData is not None and self.plot_item.yData is not None:
                if self.plot_item.domain == AssayDomainType.DEPTH:
                    min_ = min(self.plot_item.xData)
                    max_ = max(self.plot_item.xData)
                else:
                    min_ = min(self.plot_item.yData)
                    max_ = max(self.plot_item.yData)
                self.min_color_ramp_param.setValue(
                    min_, self._update_plot_item_color_ramp
                )
                self.min_color_ramp_param.setDefault(min_)
                self.max_color_ramp_param.setValue(
                    max_, self._update_plot_item_color_ramp
                )
                self.max_color_ramp_param.setDefault(max_)

            self.symbol_parameter.sigValueChanged.connect(
                lambda params, changes: self.plot_item.setSymbol(changes)
                if changes
                else self.plot_item.setSymbol(None)
            )
            self.symbol_size_parameter.sigValueChanged.connect(
                lambda params, changes: self.plot_item.setSymbolSize(changes)
            )
            self.symbol_color_parameter.sigValueChanged.connect(
                lambda params, changes: self.plot_item.setSymbolBrush(changes)
            )
            self._update_plot_item_color_ramp()

    def _update_color_ramp_from_name(self) -> None:
        """
        Update displayed color ramp from chosen name

        """
        cm = pg.colormap.get(self.color_ramp_name_param.value())
        self.color_ramp_parameter.setValue(cm)

    def _update_plot_item_color_ramp(self) -> None:
        """
        Update plot item color ramp from current parameter

        """
        if self.plot_item:
            cm = self.color_ramp_parameter.value()
            pen = self.pen_params.pen
            if cm and self.pen_color_ramp_use_param.value():
                pen = cm.getPen(
                    span=(
                        self.min_color_ramp_param.value(),
                        self.max_color_ramp_param.value(),
                    ),
                    width=pen.width(),
                    orientation="horizontal"
                    if self.plot_item.domain == AssayDomainType.DEPTH
                    else "vertical",
                )
                self.plot_item.setPen(pen)
            else:
                self.plot_item.setPen(pen)

        # brush = cm.getBrush(span=(min_, max_), orientation='vertical')
        # self.plot_item.setFillLevel(min_)
        # self.plot_item.setBrush(brush)

    def copy_from_config(self, other) -> None:
        """
        Copy configuration from another configuration.
        If a plot item is associated it will be updated

        Args:
            other: configuration to be copy
        """
        super().copy_from_config(other)
        if other.uncertainty_visibility_param and self.uncertainty_visibility_param:
            self.uncertainty_visibility_param.setValue(
                other.uncertainty_visibility_param.value()
            )

        self.symbol_parameter.setValue(other.symbol_parameter.value())
        self.symbol_size_parameter.setValue(other.symbol_size_parameter.value())
        self.symbol_color_parameter.setValue(other.symbol_color_parameter.value())

        self.color_ramp_name_param.setValue(other.color_ramp_name_param.value())
        self.color_ramp_parameter.setValue(other.color_ramp_parameter.value())
        self.min_color_ramp_param.setValue(
            other.min_color_ramp_param.value(), self._update_plot_item_color_ramp
        )
        self.max_color_ramp_param.setValue(
            other.max_color_ramp_param.value(), self._update_plot_item_color_ramp
        )
        self.pen_color_ramp_use_param.setValue(other.pen_color_ramp_use_param.value())
        self._update_plot_item_color_ramp()

        if self.extended_config:
            self.extended_config.copy_from_config(other.extended_config)
            if self.as_extended != other.as_extended:
                self._switch_to_extended()

    def _switch_to_extended(self) -> None:
        plot_widget = self.get_current_plot_widget()
        self.as_extended = not self.as_extended

        if self.assay and self.plot_item and plot_widget:
            plot_widget.removeItem(self.plot_item)

            for plot_item in self._uncertainty_plot_items:
                plot_widget.removeItem(plot_item)

            if not self.as_extended:
                plot_widget.removeItem(self.extended_config.plot_item)
                self.plot_item = AssayPlotItemFactory.create_discrete_plot_items(
                    plot_widget, self.assay, self.column.name, self
                )
                for plot_item in self._uncertainty_plot_items:
                    visible = (
                        self.uncertainty_visibility_param.value()
                        and self.visibility_param.value()
                    )
                    plot_item.setVisible(visible)
                self.update_plot_item_unit(
                    self.column.unit, self.unit_parameter.value()
                )
            else:
                self.plot_item = AssayPlotItemFactory.create_extended_plot_item_from_discrete_numerical(
                    plot_widget,
                    self.assay,
                    self.column.name,
                    self.extended_config,
                )
                self.extended_config.update_plot_item_unit(
                    self.column.unit, self.unit_parameter.value()
                )
            self.plot_item.setVisible(self.visibility_param.value())

        # Update parameters
        self.visibility_param.clearChildren()
        self.add_children_to_root_param(self.visibility_param)

    def _extended_unit_updated(self) -> None:
        self.unit_parameter.setValue(self.extended_config.unit_parameter.value())

    def _update_conversion_unit(self) -> None:
        if self.plot_item and not self.as_extended:
            new_conversion_unit = self.unit_parameter.value()
            if not pint_utilities.can_convert(
                new_conversion_unit, self._conversion_unit
            ):
                self.unit_parameter.setValue(self._conversion_unit)
                return

            if new_conversion_unit != self._conversion_unit:
                self.update_plot_item_unit(self._conversion_unit, new_conversion_unit)
                if self.extended_config:
                    self.extended_config.unit_parameter.setValue(new_conversion_unit)

    def update_plot_item_unit(self, from_unit: str, to_unit: str) -> None:
        xData, yData = self.plot_item.getOriginalDataset()
        if self.assay.assay_definition.domain == AssayDomainType.DEPTH:
            xData = pint_utilities.unit_conversion(from_unit, to_unit, xData)
        else:
            yData = pint_utilities.unit_conversion(from_unit, to_unit, yData)
        self.plot_item.setData(x=xData, y=yData)

        for plot_item in self._uncertainty_plot_items:
            opts = plot_item.opts
            if self.assay.assay_definition.domain == AssayDomainType.DEPTH:
                opts["x"] = pint_utilities.unit_conversion(
                    from_unit, to_unit, opts["x"]
                )
                opts["right"] = pint_utilities.unit_conversion(
                    from_unit, to_unit, opts["right"]
                )
                opts["left"] = pint_utilities.unit_conversion(
                    from_unit, to_unit, opts["left"]
                )
            else:
                opts["y"] = pint_utilities.unit_conversion(
                    from_unit, to_unit, opts["y"]
                )
                opts["top"] = pint_utilities.unit_conversion(
                    from_unit, to_unit, opts["top"]
                )
                opts["bottom"] = pint_utilities.unit_conversion(
                    from_unit, to_unit, opts["bottom"]
                )

            plot_item.setData(**opts)

        self._conversion_unit = to_unit
