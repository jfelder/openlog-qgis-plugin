import pyqtgraph as pg
import pyqtgraph.parametertree as ptree
import pyqtgraph.parametertree.parameterTypes as pTypes
from qgis.PyQt.QtGui import QColor, QPen
from qgis.PyQt.QtWidgets import QWidget

from openlog.core import pint_utilities
from openlog.datamodel.assay.generic_assay import AssayColumn, GenericAssay
from openlog.toolbelt import PlgTranslator


class AssayColumnVisualizationConfig:
    def __init__(self, column: AssayColumn):
        """
        Store visualization configuration for an assay column.
        Can also access plot item if configuration created from visualization widget

        Configuration supported :
        - visibility_param (bool) : assay column visibility (default : True)
        - pen_params (QPen) : assay column pen (default : Black

        Args:
            column: (AssayColumn) assay column
        """

        # translation
        self.tr = PlgTranslator().tr

        self.column = column
        self.hole_id = ""
        self.hole_display_name = ""
        self.assay_name = ""
        self.assay_display_name = ""
        self.plot_item = None
        self.assay = None
        self.plot_widget = None
        self.plot_stacked = None

        pen = QPen(QColor("black"))
        pen.setWidth(2)
        pen.setCosmetic(
            True
        )  # Need to add cosmetic option or graph is not correctly displayed

        # Default not working with pyqtgraph for pen attributes
        self.pen_params = pTypes.PenParameter(name=self.tr("Line"), value=pen)
        self.pen_params.setOpts(expanded=False)
        name = f"{column.name} ({column.unit})" if column.unit else column.name
        self.visibility_param = ptree.Parameter.create(
            name=name, type="bool", value=True, default=True
        )
        # Connection to parameter changes
        self.pen_params.sigValueChanged.connect(self._pen_updated)
        self.visibility_param.sigValueChanged.connect(self._visibility_changed)

        self.unit_group = ptree.Parameter.create(name=self.tr("Unit"), type="group")
        self.unit_group.setOpts(expanded=False)
        self.unit_parameter = pTypes.SimpleParameter(
            name=self.tr("Conversion"),
            type="str",
            value=column.unit,
            default=column.unit,
        )
        self.unit_group.addChild(self.unit_parameter)
        self._conversion_unit = column.unit
        self.unit_parameter.sigValueChanged.connect(self._update_conversion_unit)

    def set_assay(self, assay: GenericAssay) -> None:
        self.assay = assay

    def update_name_for_stacked_config(self) -> None:
        """
        Update name for stacked config display

        """

        name = f"{self.assay_display_name};{self.column.name};{self.hole_display_name}"
        name = f"{name} ({self.column.unit})" if self.column.unit else name
        self.visibility_param.setName(name)

    def set_plot_item(self, plot_item: pg.PlotDataItem) -> None:
        """
        Define plot item containing current assay data

        Args:
            plot_item: pg.PlotDataItem
        """
        self.plot_item = plot_item
        if self.plot_item:
            # Define current parameters
            self.plot_item.setPen(self.pen_params.pen)
            self.plot_item.setVisible(self.visibility_param.value())

    def get_current_plot_widget(self) -> pg.PlotWidget:
        """
        Get current plot widget used to display plot item

        Returns:
            pg.PlotWidget: current plot widget, None is none used

        """
        if self.plot_widget and self.plot_widget.isVisible():
            return self.plot_widget
        if self.plot_stacked and self.plot_stacked.isVisible():
            return self.plot_stacked
        return None

    def stack_widget(self, plot_stacked: pg.PlotWidget) -> None:
        """
        Add current plot item to a new plot widget and remove them from current plot widget

        Args:
            plot_stacked (pg.PlotWidget): plot widget where item are added
        """
        if self.plot_widget and self.plot_item:
            self.plot_widget.removeItem(self.plot_item)
            plot_stacked.addItem(self.plot_item)
            self.plot_widget.setVisible(False)

    def unstack_widget(self, plot_stacked: pg.PlotWidget) -> None:
        """
        Remove current plot item from a stacked plot widget and add them to current plot widget

        Args:
            plot_stacked (pg.PlotWidget): plot widget where items are available
        """
        if self.plot_widget and self.plot_item:
            plot_stacked.removeItem(self.plot_item)
            self.plot_widget.addItem(self.plot_item)
            self.plot_widget.setVisible(self.visibility_param.value())

    def _visibility_changed(self) -> None:
        """
        Update plot visibility when parameter is changed

        """
        if self.plot_item:
            self.plot_item.setVisible(self.visibility_param.value())

            if self.plot_widget and not self.plot_stacked.isVisible():
                self.plot_widget.setVisible(self.visibility_param.value())

    def _update_conversion_unit(self) -> None:
        # Specific implementation for extended or discrete configuration
        pass

    def _pen_updated(self) -> None:
        """
        Update plot pen when parameter is changed

        """
        if self.plot_item:
            self.plot_item.setPen(self.pen_params.pen)

    def get_pyqtgraph_param(self) -> ptree.Parameter:
        """
        Get pyqtgraph param to display in pyqtgraph ParameterTree

        Returns: ptree.Parameter containing all configuration params

        """
        params = self.visibility_param
        params.clearChildren()
        self.add_children_to_root_param(params)
        return params

    def add_children_to_root_param(self, params: ptree.Parameter):
        if pint_utilities.is_pint_unit(self.column.unit):
            params.addChild(self.unit_group)
        params.addChild(self.pen_params)

    def create_configuration_widgets(self, parent: None) -> [QWidget]:
        """
        Create widgets needed for configuration not available as pyqtgraph param

        Args:
            parent: parent QWidget

        Returns: [QWidget] list of QWidget for specific configuration

        """
        return []

    def copy_from_config(self, other) -> None:
        """
        Copy configuration from another configuration.
        If a plot item is associated it will be updated

        Args:
            other: configuration to be copy
        """
        self.pen_params.setValue(other.pen_params.pen)
        self.visibility_param.setValue(other.visibility_param.value())
        self.unit_parameter.setValue(other.unit_parameter.value())


class AssayVisualizationConfig:
    def __init__(self, hole_id: str, assay: str):
        """
        Store visualization configuration for an assay.
        Can also access plot and plot item if configuration created from visualization widget

        Configuration supported :
        - is_visible (bool) : assay visibility (default : True)

        Args:
            hole_id: (str) collar hole id
            assay: (str) assay name
        """

        # translation
        self.tr = PlgTranslator().tr

        self.hole_id = hole_id
        self.hole_display_name = ""
        self.assay_name = assay
        self.assay_display_name = ""
        self.is_visible = True
        self.plot_stacked = None
        self.column_config = {}

        self.stacked_param = ptree.Parameter.create(
            name=self.tr("Stacked"),
            type="bool",
            value=True,
            default=True,
        )
        # Connection to parameter changes
        self.stacked_param.sigValueChanged.connect(self._stacked_changed)

    def get_pyqtgraph_param(self) -> ptree.Parameter:
        """
        Get pyqtgraph param to display in pyqtgraph ParameterTree

        Returns: ptree.Parameter containing all configuration params

        """
        if len(self.column_config) > 1:
            return self.stacked_param
        else:
            return None

    def _stacked_changed(self) -> None:
        if self.plot_stacked:
            self.plot_stacked.setVisible(self.stacked_param.value())
            for col, col_config in self.column_config.items():
                if self.stacked_param.value():
                    col_config.stack_widget(self.plot_stacked)
                else:
                    col_config.unstack_widget(self.plot_stacked)

    def set_plot_stacked(self, plot: pg.PlotWidget) -> None:
        """
        Define plot containing current assay

        Args:
            plot: pg.PlotWidget
        """
        self.plot_stacked = plot

    def add_column_config(self, config: AssayColumnVisualizationConfig) -> None:
        """
        Add assay column configuration

        Args:
            config: AssayColumnVisualizationConfig
        """
        self.column_config[config.column.name] = config

    def copy_from_config(self, other) -> None:
        """
        Copy configuration from another configuration.
        If a plot item is associated it will be updated

        Args:
            other: configuration to be copy
        """
        self.is_visible = other.is_visible
        self.stacked_param.setValue(other.stacked_param.value())

        # Don't create column configuration but copy common
        for key, config in self.column_config.items():
            if key in other.column_config:
                config.copy_from_config(other.column_config[key])


class AssayVisualizationConfigList:
    class UnavailableDefaultConfig(Exception):
        pass

    def __init__(self):
        """
        Container of AssayVisualizationConfig.
        List can be accessed with list attributes.

        Define several method for list modification and query

        """
        self.list = []

    def clear(self) -> None:
        """
        Clear current config list.
        If plot is associated to configuration, plot is also deleted

        """
        to_remove = [i for i, val in enumerate(self.list)]
        self._remove_indexes(to_remove)

    def collar_list(self) -> [str]:
        """
        Return list of unique available hole id

        Returns: [str] unique available hole id list

        """
        return set([x.hole_id for x in self.list])

    def assay_list(self) -> [str]:
        """
        Return list of unique available assay

        Returns: [str] unique available assay list

        """
        return set([x.assay_name for x in self.list])

    def assay_display_name_list(self) -> [str]:
        """
        Return list of unique available assay display name

        Returns: [str] unique available assay display name list

        """
        return set([x.assay_display_name for x in self.list])

    def get_default_configuration(self, assay: str) -> AssayVisualizationConfig:
        """
        Get default configuration for assay

        Raises UnavailableDefaultConfig if no config available for assay

        Args:
            assay: (str) assay name

        Returns: AssayVisualizationConfig

        """
        default_config = [
            x for x in self.list if x.assay_name == assay and not x.hole_id
        ]
        if default_config:
            return default_config[0]
        else:
            raise AssayVisualizationConfigList.UnavailableDefaultConfig()

    def has_collar_assay(self, hole_id: str, assay: str) -> bool:
        """
        Check if list contains a configuration for a specific collar and assay

        Args:
            hole_id: (str) collar hole id
            assay: (str) assay name

        Returns: True if at least one assay configuration is available, False otherwise

        """
        return any(x.hole_id == hole_id and x.assay_name == assay for x in self.list)

    def has_collar(self, hole_id: str) -> bool:
        """
        Check if list contains collar

        Args:
            hole_id: (str) collar hole id

        Returns: True if at least one assay configuration is available for collar, False otherwise

        """
        return any(x.hole_id == hole_id for x in self.list)

    def has_assay(self, assay: str) -> bool:
        """
        Check if list contains assay

        Args:
            assay: (str) assay name

        Returns: True if at least one assay configuration is available for assay, False otherwise

        """
        return any(x.assay_name == assay for x in self.list)

    def remove_collar(self, hole_id: str) -> None:
        """
        Remove all configuration associated to collar.
        If plot is associated to configuration, plot is also deleted

        Args:
            hole_id: (str) collar hole id
        """
        to_remove = [i for i, val in enumerate(self.list) if val.hole_id == hole_id]
        self._remove_indexes(to_remove)

    def remove_assay(self, assay: str) -> None:
        """
        Remove all configuration associated to assay.
        If plot is associated to configuration, plot is also deleted

        Args:
            assay: (str) assay name
        """
        to_remove = [i for i, val in enumerate(self.list) if val.assay_name == assay]
        self._remove_indexes(to_remove)

    def _remove_indexes(self, to_remove: [int]) -> None:
        """
        Remove indexes from list

        Args:
            to_remove: indexes to remove
        """
        for index in reversed(to_remove):
            plot = self.list[index].plot_stacked
            if plot is not None:
                # Update visibility even if plot is deleted later
                # so we can count number of visible plot if main thread as not process event
                plot.setVisible(False)
                plot.deleteLater()

            for col, col_config in self.list[index].column_config.items():
                if col_config.plot_widget:
                    # Update visibility even if plot is deleted later
                    # so we can count number of visible plot if main thread as not process event
                    col_config.plot_widget.setVisible(False)
                    col_config.plot_widget.deleteLater()

            del self.list[index]
