from openlog.gui.assay_visualization.assay_visualization_config import (
    AssayColumnVisualizationConfig,
)
from openlog.gui.pyqtgraph.CustomBarGraphItem import CustomBarGraphItem


class AssayBarGraphItem(CustomBarGraphItem):
    def __init__(self, config: AssayColumnVisualizationConfig, **opts):
        super().__init__(**opts)
        self.config = config
