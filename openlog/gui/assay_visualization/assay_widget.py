import os
from typing import List

from qgis.core import QgsApplication
from qgis.PyQt import uic
from qgis.PyQt.QtCore import QCoreApplication, Qt
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import (
    QAbstractItemView,
    QAction,
    QCheckBox,
    QInputDialog,
    QMessageBox,
    QProgressDialog,
    QRadioButton,
    QToolBar,
    QWidget,
)

from openlog.__about__ import DIR_PLUGIN_ROOT
from openlog.datamodel.assay.generic_assay import AssayDefinition, AssayDomainType
from openlog.datamodel.connection.openlog_connection import OpenLogConnection
from openlog.gui.assay_visualization.assay_visualization_config import (
    AssayVisualizationConfig,
    AssayVisualizationConfigList,
)
from openlog.gui.assay_visualization.assay_visualization_config_tree_model import (
    AssayVisualizationConfigProxyModel,
    AssayVisualizationConfigTreeModel,
)
from openlog.gui.assay_visualization.config_factory import (
    AssayColumnVisualizationConfigFactory,
)
from openlog.gui.assay_visualization.extended.config import (
    ExtendedCategoricalAssayColumnVisualizationConfig,
)
from openlog.gui.assay_visualization.plot_grid import AssaySort, PlotGrid
from openlog.gui.assay_visualization.plot_item_factory import AssayPlotItemFactory
from openlog.gui.assay_visualization.stacked.stacked_config import (
    StackedConfiguration,
    StackedConfigurationList,
)
from openlog.toolbelt import PlgLogger, PlgTranslator
from openlog.toolbelt.checkable_combobox_input_dialog import (
    CheckableComboBoxInputDialog,
)


class AssayWidget(QWidget):
    def __init__(
        self,
        domain: AssayDomainType,
        parent: QWidget = None,
        openlog_connection: OpenLogConnection = None,
    ):
        """
        Widget to display assay from an OpenLogConnection

        Args:
            parent: parent QWidget
            domain: AssayDomainType assay domain type
            openlog_connection: OpenLogConnection used to get assays
        """
        super().__init__(parent)
        self.domain = domain
        self._openlog_connection = openlog_connection
        self.log = PlgLogger().log

        uic.loadUi(os.path.join(os.path.dirname(__file__), "assay_widget.ui"), self)

        self._visualization_config = AssayVisualizationConfigList()
        self._stacked_config = StackedConfigurationList()

        # Add/Remove assay actions
        self.add_assay_action = QAction(
            QIcon(str(DIR_PLUGIN_ROOT / "resources" / "images" / "add_assay.svg")),
            self.tr("Add"),
            self,
        )
        self.add_assay_action.triggered.connect(self._add_new_assay)

        self.remove_assay_action = QAction(
            QIcon(QgsApplication.iconPath("mActionRemove.svg")),
            self.tr("Remove"),
            self,
        )
        self.remove_assay_action.triggered.connect(self._remove_selected_assay)
        self.remove_assay_action.setEnabled(False)

        self.sort_by_collar_button = QRadioButton(self.tr("Sort by collar"))
        self.sort_by_collar_button.setIcon(
            QIcon(str(DIR_PLUGIN_ROOT / "resources" / "images" / "sort_collar.svg"))
        )
        self.sort_by_collar_button.setChecked(True)
        self.sort_by_collar_button.clicked.connect(self._assay_sort_updated)

        self.sort_by_assay_button = QRadioButton(self.tr("Sort by source"))
        self.sort_by_assay_button.setIcon(
            QIcon(str(DIR_PLUGIN_ROOT / "resources" / "images" / "sort_assay.svg"))
        )
        self.sort_by_assay_button.clicked.connect(self._assay_sort_updated)

        self.flat_grid_checkbox = QCheckBox(self.tr("Transpose"))
        self.flat_grid_checkbox.setIcon(
            QIcon(str(DIR_PLUGIN_ROOT / "resources" / "images" / "flat_grid.svg"))
        )
        self.flat_grid_checkbox.clicked.connect(
            lambda checked: self._plot_grid.enable_flat_grid(checked)
        )

        # Plot tree widget
        self.plot_tree_widget.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.plot_tree_widget.setSortingEnabled(True)
        self.plot_tree_widget.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self._plot_grid = PlotGrid(
            domain,
            self._visualization_config,
            self._stacked_config,
            self._openlog_connection,
            self,
        )

        self.toolbar = QToolBar(self)
        self.toolbar.addAction(self.add_assay_action)
        self.toolbar.addAction(self.remove_assay_action)
        self.toolbar.addSeparator()
        self.toolbar.addWidget(self.sort_by_assay_button)
        self.toolbar.addWidget(self.sort_by_collar_button)
        self.toolbar.addWidget(self.flat_grid_checkbox)

        self.toolbar.addSeparator()
        self._plot_grid.add_action_to_toolbar(self.toolbar)
        self.toolbar_hboxlayout.addWidget(self.toolbar)

        self.plot_grid_scrollarea.setWidget(self._plot_grid)

        # Visualization configuration
        self._visualization_config_model = AssayVisualizationConfigTreeModel(
            self._visualization_config,
            self._stacked_config,
            self._openlog_connection,
            self,
        )
        self._proxy_mdl = AssayVisualizationConfigProxyModel(self)
        self._proxy_mdl.setSourceModel(self._visualization_config_model)
        self.plot_tree_widget.setModel(self._proxy_mdl)
        self.plot_tree_widget.selectionModel().selectionChanged.connect(
            self._tree_selection_changed
        )
        self.plot_tree_widget.header().sortIndicatorChanged.connect(
            self._sort_order_updated
        )

        self._check_state_connect = None
        self._connect_check_state_signal()

        self.plot_selection_splitter.setStretchFactor(0, 0)
        self.plot_selection_splitter.setStretchFactor(1, 1)

        self.assay_config_widget.apply_to_all.connect(self._apply_to_all)
        self.assay_config_widget.apply_to_visible.connect(self._apply_to_visible)
        self.assay_config_widget.apply_to_selected.connect(self._apply_to_selected)

        self.stacked_config_creation_widget.add_stacked_config.connect(
            self._add_current_stacked_config
        )

        self.set_openlog_connection(self._openlog_connection)

    def set_openlog_connection(self, openlog_connection: OpenLogConnection):
        """
        Define used OpenLogConnection

        Args:
            openlog_connection: OpenLogConnection
        """
        self._clear_config()
        self._openlog_connection = openlog_connection
        self._visualization_config_model.openlog_connection = openlog_connection
        self.add_assay_action.setEnabled(self._openlog_connection is not None)
        self._plot_grid.set_openlog_connection(openlog_connection)

        self.assay_config_widget.setVisible(False)
        self.stacked_config_creation_widget.setVisible(False)
        self.stacked_config_widget.setVisible(False)

        if self._openlog_connection is not None:
            # For now collar selection is done by collar feature synchronization
            collar_layer = (
                self._openlog_connection.get_layers_iface().get_collar_layer()
            )
            if collar_layer is not None:
                collar_layer.selectionChanged.connect(self._collar_feature_selected)

    def _connect_check_state_signal(self):
        """
        Connect check state signal for automatic limit and sync plot definition

        """
        self._check_state_connect = (
            self._visualization_config_model.itemCheckStateChanged.connect(
                self._check_state_changed
            )
        )

    def _disconnect_check_state_signal(self):
        """
        Disconnect check state signal for automatic limit and sync plot definition

        """
        if self._check_state_connect:
            self._visualization_config_model.itemCheckStateChanged.disconnect(
                self._check_state_connect
            )
            self._check_state_connect = None

    def _clear_config(self) -> None:
        """
        Clear current visualization configuration.
        If plot is associated to configuration, plot is also deleted

        """
        self._visualization_config.clear()
        self._visualization_config_model.refresh()
        for assay in self._visualization_config_model.get_available_assay():
            self._visualization_config_model.remove_assay(assay)
        for collar in self._visualization_config_model.get_available_collar():
            self._visualization_config_model.remove_collar(collar)

    def _add_new_assay(self) -> None:
        """
        Ask user for assay selection

        """
        displayed_assay = self._visualization_config_model.get_available_assay()
        assay_definitions = (
            self._openlog_connection.get_assay_iface().get_all_available_assay_definitions()
        )

        # Display only not displayed assay
        selected_hole_id = self._get_selected_hole_id()

        assay_list = [
            assay
            for assay in assay_definitions
            if assay.variable not in displayed_assay
            and assay.domain == self.domain
            and self._openlog_connection.get_assay_iface().is_assay_available_for_collars(
                selected_hole_id, assay.variable
            )
        ]

        assay_name_list = [assay.variable for assay in assay_list]
        assay_display_name_list = [assay.display_name for assay in assay_list]

        dialog = CheckableComboBoxInputDialog(
            assay_name_list,
            assay_display_name_list,
            self,
            self.tr("Downhole data selection"),
            self.tr("Select downhole data"),
        )
        dialog.clear_selection()
        if dialog.exec():

            # Disconnect check state signal to avoid multiple automatic limits calculation
            self._disconnect_check_state_signal()

            assay_list = dialog.get_selected_values()
            self._add_selected_assay_and_collar(
                assay_list, selected_hole_id, assay_definitions
            )

            # Restore check state signal connection for automatic limits calculation
            self._connect_check_state_signal()
            self._check_state_changed()

    def _add_selected_assay_and_collar(
        self,
        assay_list: List[str],
        selected_hole_id: List[str],
        assay_definitions: List[AssayDefinition],
    ) -> None:
        """
        Add selected assay and collar to current visualization widget :
        - create default configuration for each assay
        - add configuration to

        Args:
            assay_list: (List[str]) selected assay names
            selected_hole_id: (List[str]) selected hole id
            assay_definitions: (List[AssayDefinition]) available assay definitions
        """
        # Add configuration for each selected collar
        hole_len = len(selected_hole_id)
        assay_len = len(assay_list)
        if hole_len and assay_len:
            progress_dialog = QProgressDialog(
                self.tr("Importing collar data"),
                self.tr("Cancel"),
                0,
                hole_len * assay_len,
                self,
            )
            progress_dialog.setWindowModality(Qt.WindowModal)
            progress_dialog.show()

            i = 0
            for assay_name in assay_list:
                assay_def = [
                    definition
                    for definition in assay_definitions
                    if definition.variable == assay_name
                ][0]
                display_name = assay_def.display_name
                title = (
                    self.tr("Time domain downhole data :{0}").format(display_name)
                    if self.domain == AssayDomainType.TIME
                    else self.tr("Depth domain downhole data:{0}").format(display_name)
                )
                progress_dialog.setWindowTitle(title)

                # Add assay to tree model
                self._visualization_config_model.add_assay(assay_def)

                # Create default configuration
                self._create_default_assay_visualization_config(assay_name)
                for hole_id in selected_hole_id:
                    progress_dialog.setValue(i)
                    progress_dialog.setLabelText(
                        self.tr("Importing {0} downhole data").format(
                            self._openlog_connection.get_read_iface().get_collar_display_name(
                                hole_id
                            )
                        )
                    )
                    self._add_visualization_config(hole_id, assay_name)

                    if progress_dialog.wasCanceled():
                        break
                    i = i + 1
                    QCoreApplication.processEvents()

            progress_dialog.hide()
            progress_dialog.deleteLater()

            # Refresh displayed visualization
            self._visualization_config_model.refresh()

    def _create_default_assay_visualization_config(self, assay_name: str) -> None:
        # Get assay from connection
        assay = self._openlog_connection.get_assay_iface().get_assay("", assay_name)

        # Create assay visualization configuration
        assay_visualization_config = AssayVisualizationConfig(
            hole_id="", assay=assay_name
        )
        assay_visualization_config.is_visible = True
        assay_visualization_config.assay_display_name = (
            assay.assay_definition.display_name
        )

        # Create configuration for each assay column
        for col in assay.assay_definition.get_plottable_columns().keys():
            config = AssayColumnVisualizationConfigFactory().create_config(
                assay.assay_definition, col
            )
            config.assay_display_name = assay.assay_definition.display_name
            assay_visualization_config.add_column_config(config)

        self._visualization_config.list.append(assay_visualization_config)

    def _remove_selected_assay(self) -> None:
        """
        Remove selected assay of tree model

        """
        removed_config = []
        removed_stacked_config = []
        indexes = self.plot_tree_widget.selectionModel().selectedIndexes()
        for proxy_index in indexes:
            index = self._proxy_mdl.mapToSource(proxy_index)
            if self._visualization_config_model.is_assay(index):
                config = self._visualization_config_model.data(
                    index, self._visualization_config_model.CONFIG_ROLE
                )
                removed_config.append(config)

            if self._visualization_config_model.is_stacked(index):
                config = self._visualization_config_model.data(
                    index, self._visualization_config_model.CONFIG_ROLE
                )
                removed_stacked_config.append(config)

        for config in removed_config:
            # Remove from configuration
            self._visualization_config.remove_assay(config.assay_name)
            # Remove from tree model
            self._visualization_config_model.remove_assay(config.assay_name)

        for config in removed_stacked_config:
            # Remove from configuration
            self._stacked_config.remove_config(config)
            # Remove from tree model
            self._visualization_config_model.remove_stacked(config)

        # Refresh displayed visualization
        self._visualization_config_model.refresh()

        self._check_state_changed()

    def _collar_feature_selected(self) -> None:
        """
        Update configuration with selected collar in map

        """
        selected_hole = self._get_selected_hole_id()
        self._remove_unavailable_collar(selected_hole)
        self._add_or_update_selected_collar(selected_hole)

    def _remove_unavailable_collar(self, selected_hole: [str]) -> None:
        """
        Remove unavailable collar in tree model and visualization configuration

        Args:
            selected_hole: [str] selected collar ids
        """
        unavailable_collar = [
            hole_id
            for hole_id in self._visualization_config_model.get_available_collar()
            if hole_id not in selected_hole
        ]
        for hole_id in unavailable_collar:
            self._visualization_config.remove_collar(hole_id)
            self._visualization_config_model.remove_collar(hole_id)

    def _add_or_update_selected_collar(self, selected_hole: [str]) -> None:
        """
        Add or update selected collar for visualization config add if needed

        Args:
            selected_hole: [str]  selected collar ids
        """
        hole_len = len(selected_hole)

        # Disconnect check state signal to avoid multiple automatic limits calculation
        self._disconnect_check_state_signal()

        if hole_len:

            progress_dialog = QProgressDialog(
                self.tr("Importing collar assays"), self.tr("Cancel"), 0, hole_len, self
            )
            title = (
                self.tr("Time assays")
                if self.domain == AssayDomainType.TIME
                else self.tr("Depth assays")
            )
            progress_dialog.setWindowTitle(title)
            progress_dialog.setWindowModality(Qt.WindowModal)
            progress_dialog.show()
            i = 0
            for hole_id in selected_hole:
                progress_dialog.setValue(i)
                progress_dialog.setLabelText(
                    self.tr("Importing {0} assays").format(
                        self._openlog_connection.get_read_iface().get_collar_display_name(
                            hole_id
                        )
                    )
                )
                if self._visualization_config.has_collar(hole_id):
                    self._update_collar(hole_id)
                else:
                    self._add_collar(hole_id)
                if progress_dialog.wasCanceled():
                    break
                i = i + 1
                QCoreApplication.processEvents()

            progress_dialog.hide()
            progress_dialog.deleteLater()

        self._visualization_config_model.refresh()

        # Restore check state signal connection for automatic limits calculation
        self._connect_check_state_signal()
        self._check_state_changed()

    def _update_collar(self, hole_id: str) -> None:
        """
        Update collar visualization configuration with added assays

        Args:
            hole_id: (str) collar hole id
        """
        # Add only assay not already available
        added_assay = [
            assay
            for assay in self._visualization_config_model.get_available_assay()
            if not self._visualization_config.has_collar_assay(hole_id, assay)
        ]
        for assay in added_assay:
            self._add_visualization_config(hole_id, assay)

    def _add_collar(self, hole_id: str) -> None:
        """
        Add collar visualization configuration

        Args:
            hole_id: (str) collar hole id
        """
        # Add all available assays
        for assay_name in self._visualization_config_model.get_available_assay():
            self._add_visualization_config(hole_id, assay_name)

    def _add_visualization_config(self, hole_id: str, assay_name: str) -> None:
        """
        Add a new visualization configuration for collar and assay

        Args:
            hole_id: (str) collar hole id
            assay_name: (str) assay name
        """
        if not self._openlog_connection.get_assay_iface().is_assay_available_for_collar(
            hole_id, assay_name
        ):
            return None

        # Add collar to tree model
        self._visualization_config_model.add_collar(hole_id)

        # Get assay from connection
        assay = self._openlog_connection.get_assay_iface().get_assay(
            hole_id, assay_name
        )

        # Create assay visualization configuration
        assay_visualization_config = AssayVisualizationConfig(
            hole_id=hole_id, assay=assay_name
        )
        assay_visualization_config.hole_display_name = (
            self._openlog_connection.get_read_iface().get_collar_display_name(hole_id)
        )
        assay_visualization_config.assay_display_name = (
            assay.assay_definition.display_name
        )

        # Default visibility
        visible_assays = self._visualization_config_model.get_available_assay(True)
        is_visible = assay_name in visible_assays
        assay_visualization_config.is_visible = is_visible

        # Create configuration for each assay column
        default_config = self._visualization_config.get_default_configuration(
            assay_name
        )

        # Create plot widget for assay and collar
        plot_stacked = self._plot_grid.add_plot_widget(assay)
        assay_visualization_config.set_plot_stacked(plot_stacked)
        plot_stacked.setVisible(
            is_visible and assay_visualization_config.stacked_param.value()
        )

        for col, col_def in assay.assay_definition.get_plottable_columns().items():
            # Create plot widget for assay and collar
            plot = self._plot_grid.add_plot_widget(assay)
            plot.setVisible(
                is_visible and not assay_visualization_config.stacked_param.value()
            )

            config = AssayColumnVisualizationConfigFactory().create_config(
                assay.assay_definition, col
            )
            config.hole_id = assay.hole_id
            config.hole_display_name = (
                self._openlog_connection.get_read_iface().get_collar_display_name(
                    assay.hole_id
                )
            )
            config.assay_name = assay.assay_definition.variable
            config.assay_display_name = assay.assay_definition.display_name
            config.plot_widget = plot
            config.plot_stacked = plot_stacked
            used_plot = (
                plot_stacked
                if assay_visualization_config.stacked_param.value()
                else plot
            )
            AssayPlotItemFactory().create_plot_items(used_plot, assay, col, config)
            used_plot.getViewBox().autoRange()
            assay_visualization_config.add_column_config(config)

            # For categorical assay, default configuration doesn't have category
            if isinstance(config, ExtendedCategoricalAssayColumnVisualizationConfig):
                # Copy of configuration into default configuration
                default_config_col = default_config.column_config[col]

                # get not available bar symbology
                for (
                    key,
                    symb_config,
                ) in config.bar_symbology_map.category_symbology_map.items():
                    if not default_config_col.bar_symbology_map.has_category(key):
                        default_config_col.bar_symbology_map.add_category(
                            key, symb_config
                        )

            plot.set_assay_column_config(config)

        # Use default configuration if available
        try:
            assay_visualization_config.copy_from_config(
                self._visualization_config.get_default_configuration(assay_name)
            )
        except AssayVisualizationConfigList.UnavailableDefaultConfig:
            self.log(
                message=self.tr(
                    "No default configuration available for assay {}"
                ).format(assay_name)
            )

        plot_stacked.set_assay_config(assay_visualization_config)
        self._visualization_config.list.append(assay_visualization_config)

    def _check_state_changed(self) -> None:
        """
        Update sync plot and define automatic limits when selection is changed

        """
        self._plot_grid.update_sync_plot()
        self._plot_grid.automatic_limits()
        self._plot_grid.update_empty_cells_visibility()

    def _assay_sort_updated(self) -> None:
        """
        Update plot grid sort

        """
        if self.sort_by_assay_button.isChecked():
            sort = AssaySort.ASSAY_NAME
        elif self.sort_by_collar_button.isChecked():
            sort = AssaySort.HOLE_ID
        self._plot_grid.set_assay_sort(sort)

    def _sort_order_updated(self) -> None:
        """
        Update plot grid row/col sort

        """
        sort_order = self.plot_tree_widget.header().sortIndicatorOrder()
        self._plot_grid.set_sort_order(sort_order)

    def _get_selected_hole_id(self) -> [str]:
        """
        Get selected collar from OpenLogConnection collar layer

        Returns: [str] list of selected collar hole id

        """
        return (
            self._openlog_connection.get_layers_iface().get_selected_collar_from_layer()
        )

    def _tree_selection_changed(self) -> None:
        """
        Defined remove action enable from current tree model selection

        """
        assay_selected = False
        stacked_selected = False
        indexes = self.plot_tree_widget.selectionModel().selectedIndexes()
        for proxy_index in indexes:
            index = self._proxy_mdl.mapToSource(proxy_index)
            assay_selected = self._visualization_config_model.is_assay(index)
            stacked_selected = self._visualization_config_model.is_stacked(index)

        self.remove_assay_action.setEnabled(assay_selected or stacked_selected)

        self._update_assay_config_widget_from_selection()
        self._update_stacked_config_widget_from_selection()
        self._update_stacked_config_creation_widget_from_selection()

    def _update_assay_config_widget_from_selection(self) -> None:
        """
        Update current assay config widget from assay selection

        """
        self.assay_config_widget.setVisible(False)
        indexes = self.plot_tree_widget.selectionModel().selectedIndexes()
        if len(indexes) == 1:
            config = indexes[0].data(self._visualization_config_model.CONFIG_ROLE)
            if isinstance(config, AssayVisualizationConfig):
                self.assay_config_widget.set_assay_config(config)
                self.assay_config_widget.setVisible(True)

    def _apply_to_all(self) -> None:
        """
        Apply selected assay visualization configuration to all collar and define configuration as default

        """
        selected_config = self.assay_config_widget.assay_config
        for config in self._visualization_config.list:
            # Update configuration if same assay
            if config.assay_name == selected_config.assay_name:
                config.copy_from_config(selected_config)

    def _apply_to_selected(self) -> None:
        """
        Apply selected assay visualization configuration to selected collar

        """
        selected_config = self.assay_config_widget.assay_config
        for config in self._visualization_config.list:
            # Update configuration if same assay but not if default configuration
            if config.assay_name == selected_config.assay_name and config.hole_id:
                config.copy_from_config(selected_config)

    def _apply_to_visible(self) -> None:
        """
        Apply selected assay visualization configuration to visible collar

        """
        selected_config = self.assay_config_widget.assay_config
        for config in self._visualization_config.list:
            # Update configuration if same assay but not if default configuration or collar not visible
            if (
                config.assay_name == selected_config.assay_name
                and config.hole_id
                and config.is_visible
            ):
                config.copy_from_config(selected_config)

    def _update_stacked_config_widget_from_selection(self) -> None:
        """
        Update current stacked config widget from assay selection

        """
        self.stacked_config_widget.setVisible(False)
        indexes = self.plot_tree_widget.selectionModel().selectedIndexes()
        if len(indexes) == 1:
            config = indexes[0].data(self._visualization_config_model.CONFIG_ROLE)
            if isinstance(config, StackedConfiguration):
                self.stacked_config_widget.set_stacked_config(config)
                self.stacked_config_widget.setVisible(True)

    def _update_stacked_config_creation_widget_from_selection(self):
        """
        Update current stacked config creation widget from assay column selection

        """
        self.stacked_config_creation_widget.setVisible(False)
        stacked_config = self._stacked_config_from_selected_indexes()

        if len(stacked_config.config_list) > 1:
            self.stacked_config_creation_widget.set_stacked_configuration(
                stacked_config
            )
            self.stacked_config_creation_widget.setVisible(True)

    def _stacked_config_from_selected_indexes(self) -> StackedConfiguration:
        """
        Create StackedConfiguration from treeview selected indexes

        Returns: StackedConfiguration

        """
        indexes = self.plot_tree_widget.selectionModel().selectedIndexes()
        stacked_config = StackedConfiguration()
        for proxy_index in indexes:
            index = self._proxy_mdl.mapToSource(proxy_index)
            self._extract_column_config_from_index(index, stacked_config)
        return stacked_config

    def _extract_column_config_from_index(self, index, stacked_config):
        """

        Args:
            index:
            stacked_config:
        """
        config = index.data(self._visualization_config_model.CONFIG_ROLE)
        if isinstance(config, AssayVisualizationConfig):
            # Add only configuration with defined hole_id
            if config.hole_id:
                for colum in config.column_config.values():
                    # Check if config already available
                    if not stacked_config.config_list.count(colum):
                        stacked_config.config_list.append(colum)
        # Extract configuration from children
        for i in range(0, self._visualization_config_model.rowCount(index)):
            child = self._visualization_config_model.index(i, 0, index)
            self._extract_column_config_from_index(child, stacked_config)

    def _add_current_stacked_config(self) -> None:
        """
        Add stacked configuration from current config creation widget

        """
        stacked_config = self.stacked_config_creation_widget.get_stacked_configuration()
        ok = True
        while ok:
            name, ok = QInputDialog.getText(
                self, self.tr("New stacked plot"), self.tr("Stacked name")
            )
            if name:
                used_stacked_config_names = [c.name for c in self._stacked_config.list]
                if name not in used_stacked_config_names:
                    stacked_config.name = name
                    self._add_stacked_config_plot(stacked_config)
                    break
                else:
                    QMessageBox.warning(
                        self,
                        self.tr("Invalid stacked name"),
                        self.tr("Stacked name already used."),
                    )

    def _add_stacked_config_plot(self, stacked_config: StackedConfiguration) -> None:
        """
        Add stacked configuration plot and save StackedConfiguration to model

        Args:
            stacked_config: StackedConfiguration
        """
        # Create plot widget
        plot = self._plot_grid.add_stacked_plot_widget()
        stacked_config.set_plot(plot)

        copy_config = []
        # Create configuration for each assay column
        for config in stacked_config.config_list:
            # Get assay from connection
            assay = self._openlog_connection.get_assay_iface().get_assay(
                config.hole_id, config.assay_name
            )
            # Create new configuration
            new_config = AssayColumnVisualizationConfigFactory().create_config(
                assay.assay_definition, config.column.name
            )
            # Copy style
            new_config.copy_from_config(config)
            # Define hole id and assay
            new_config.hole_id = config.hole_id
            new_config.hole_display_name = config.hole_display_name
            new_config.assay_display_name = config.assay_display_name
            new_config.assay_name = config.assay_name
            new_config.plot_stacked = plot
            # Update visibility param to display hole id
            new_config.update_name_for_stacked_config()
            # Add plot items
            AssayPlotItemFactory().create_plot_items(
                plot, assay, new_config.column.name, new_config
            )
            copy_config.append(new_config)

        plot.getViewBox().autoRange()

        # Use created config list and update model
        stacked_config.config_list = copy_config
        plot.set_stacked_config(stacked_config)

        self._stacked_config.list.append(stacked_config)
        # Refresh view
        self._visualization_config_model.refresh()
