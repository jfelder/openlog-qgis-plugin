from typing import Any

import pyqtgraph as pg
import pyqtgraph.parametertree as ptree
import pyqtgraph.parametertree.parameterTypes as pTypes

from openlog.core import pint_utilities
from openlog.datamodel.assay.generic_assay import AssayDomainType, GenericAssay
from openlog.gui.assay_visualization.assay_visualization_config import (
    AssayColumnVisualizationConfig,
)
from openlog.gui.assay_visualization.extended.bar_symbology import BarSymbologyMap
from openlog.gui.assay_visualization.extended.bar_symbology_dialog import (
    BarSymbologyDialog,
)
from openlog.gui.assay_visualization.extended.text_bar_graph_items import (
    TextBarGraphItems,
)
from openlog.gui.assay_visualization.plot_item_factory import AssayPlotItemFactory


class ExtendedAssayColumnVisualizationConfig(AssayColumnVisualizationConfig):
    def __init__(self, column: str):
        """
        Store visualization configuration for an extended assay column.
        Can also access plot item if configuration created from visualization widget

        Configuration supported :
        - visibility_param (bool) : assay column visibility (default : True)
        - pen_params (QPen) : assay column pen (default : black)
        - bar_color_param (QColor): assay column bar color (default : white)

        Args:
            column: (str) assay column name
        """
        super().__init__(column)
        self.pen_params.setName(self.tr("Bar pen"))
        self.bar_color_param = pTypes.ColorParameter(
            name=self.tr("Bar fill"), value="white", default="white"
        )

    def set_plot_item(self, plot_item: pg.PlotDataItem) -> None:
        """
        Define plot item containing current assay data

        Args:
            plot_item: pg.PlotDataItem
        """
        super().set_plot_item(plot_item)
        if self.plot_item:
            # Define current parameters
            self.plot_item.setBrush(self.bar_color_param.value())

            # Connection to parameter changes
            self.bar_color_param.sigValueChanged.connect(
                lambda params, changes: self.plot_item.setBrush(changes)
            )

    def add_children_to_root_param(self, params: ptree.Parameter):
        super().add_children_to_root_param(params)
        params.addChild(self.bar_color_param)

    def copy_from_config(self, other) -> None:
        """
        Copy configuration from another configuration.
        If a plot item is associated it will be updated

        Args:
            other: configuration to be copy
        """
        super().copy_from_config(other)
        self.pen_params.setValue(other.pen_params.pen)
        self.bar_color_param.setValue(other.bar_color_param.value())


class ExtendedNumericalAssayColumnVisualizationConfig(
    ExtendedAssayColumnVisualizationConfig
):
    def __init__(self, column: str, discrete_configuration: Any = None):
        """
        Store visualization configuration for an extended numerical assay column.
        Can also access plot item if configuration created from visualization widget

        Configuration supported :
        - visibility_param (bool) : assay_name column visibility (default : True)
        - pen_params (QPen) : assay_name column pen (default : black)
        - bar_color_param (QColor): assay_name column bar color (default : white)

        Args:
            column: (str) assay_name column name
        """
        super().__init__(column)
        self.as_discrete = False
        self.discrete_configuration = discrete_configuration
        self.btn = ptree.Parameter.create(name=self.tr("Line chart"), type="action")
        self.btn.sigActivated.connect(self._switch_to_discrete)

        # Connect to discrete config unit param change to update current unit
        if self.discrete_configuration:
            self.discrete_configuration.unit_parameter.sigValueChanged.connect(
                self._discrete_unit_updated
            )

    def set_assay(self, assay: GenericAssay) -> None:
        super().set_assay(assay)
        if self.discrete_configuration:
            self.discrete_configuration.set_assay(assay)

    def add_children_to_root_param(self, params: ptree.Parameter):
        if self.discrete_configuration:
            if self.as_discrete:
                self.btn.setName(self.tr("Bar chart"))
                params.addChild(self.btn)
                self.discrete_configuration.add_children_to_root_param(params)
            else:
                self.btn.setName(self.tr("Line chart"))
                params.addChild(self.btn)
                super().add_children_to_root_param(params)
        else:
            super().add_children_to_root_param(params)

    def copy_from_config(self, other) -> None:
        """
        Copy configuration from another configuration.
        If a plot item is associated it will be updated

        Args:
            other: configuration to be copy
        """
        super().copy_from_config(other)
        if self.discrete_configuration:
            self.discrete_configuration.copy_from_config(other.discrete_configuration)
            if self.as_discrete != other.as_discrete:
                self._switch_to_discrete()

    def _switch_to_discrete(self) -> None:
        plot_widget = self.get_current_plot_widget()
        self.as_discrete = not self.as_discrete
        if self.assay and self.plot_item and plot_widget:
            plot_widget.removeItem(self.plot_item)
            if not self.as_discrete:
                plot_widget.removeItem(self.discrete_configuration.plot_item)
                self.plot_item = (
                    AssayPlotItemFactory.create_extended_numerical_plot_items(
                        plot_widget, self.assay, self.column.name, self
                    )
                )
                self.update_plot_item_unit(
                    self.column.unit, self.unit_parameter.value()
                )
            else:
                self.plot_item = AssayPlotItemFactory.create_discrete_plot_item_from_extended_numerical(
                    plot_widget,
                    self.assay,
                    self.column.name,
                    self.discrete_configuration,
                )
                self.discrete_configuration.update_plot_item_unit(
                    self.column.unit, self.unit_parameter.value()
                )

            self.plot_item.setVisible(self.visibility_param.value())

        # Update parameters
        self.visibility_param.clearChildren()
        self.add_children_to_root_param(self.visibility_param)

    def _discrete_unit_updated(self) -> None:
        self.unit_parameter.setValue(self.discrete_configuration.unit_parameter.value())

    def _update_conversion_unit(self) -> None:
        if self.plot_item and not self.as_discrete:
            new_conversion_unit = self.unit_parameter.value()
            if not pint_utilities.can_convert(
                new_conversion_unit, self._conversion_unit
            ):
                self.unit_parameter.setValue(self._conversion_unit)
                return

            if new_conversion_unit != self._conversion_unit:
                self.update_plot_item_unit(self._conversion_unit, new_conversion_unit)
                if self.discrete_configuration:
                    self.discrete_configuration.unit_parameter.setValue(
                        new_conversion_unit
                    )

    def update_plot_item_unit(self, from_unit: str, to_unit: str) -> None:
        if self.assay.assay_definition.domain == AssayDomainType.DEPTH:
            y_val = pint_utilities.unit_conversion(
                from_unit, to_unit, self.plot_item.opts.get("width")
            )
            self.plot_item.setOpts(width=y_val)
        else:
            y_val = pint_utilities.unit_conversion(
                from_unit, to_unit, self.plot_item.opts.get("height")
            )
            self.plot_item.setOpts(height=y_val)
        self._conversion_unit = to_unit


class ExtendedNominalAssayColumnVisualizationConfig(
    ExtendedAssayColumnVisualizationConfig
):
    def __init__(self, column: str):
        """
        Store visualization configuration for an extended nominal assay column.
        Can also access plot item if configuration created from visualization widget

        Configuration supported :
        - visibility_param (bool) : assay column visibility (default : True)
        - pen_params (QPen) : assay column pen (default : black)
        - bar_color_param (QColor): assay column bar color (default : white)
        - text_color_param (QColor): assay column text color (default : black)

        Args:
            column: (str) assay column name
        """
        super().__init__(column)
        self.text_color_param = pTypes.ColorParameter(
            name=self.tr("Text color"), value="black", default="black"
        )

    def set_plot_item(self, plot_item: pg.PlotDataItem) -> None:
        """
        Define plot item containing current assay data

        Args:
            plot_item: pg.PlotDataItem
        """
        super().set_plot_item(plot_item)
        if self.plot_item:
            # Define current parameters
            self.plot_item.setTextColor(self.text_color_param.value())

            # Connection to parameter changes
            self.text_color_param.sigValueChanged.connect(
                lambda params, changes: self.plot_item.setTextColor(changes)
            )

    def add_children_to_root_param(self, params: ptree.Parameter):
        super().add_children_to_root_param(params)
        params.addChild(self.text_color_param)

    def copy_from_config(self, other) -> None:
        """
        Copy configuration from another configuration.
        If a plot item is associated it will be updated

        Args:
            other: configuration to be copy
        """
        super().copy_from_config(other)
        self.text_color_param.setValue(other.text_color_param.value())


class ExtendedCategoricalAssayColumnVisualizationConfig(AssayColumnVisualizationConfig):
    def __init__(self, column: str):
        """
        Store visualization configuration for an extended assay categorical column.
        Can also access plot item if configuration created from visualization widget

        Configuration supported :
        - visibility_param (bool) : assay column visibility (default : True)
        - pen_params (QPen) : assay column pen (default : Black)
        - bar_symbology_map : symbology for each category
        - text_visibility (bool) : assay column category visibility (default : True)
        - text_color_param (QColor): assay column category text color (default : black)

        Args:
            column: (str) assay column name
        """
        super().__init__(column)
        self.text_bar_graph_item = None
        self.pen_params.setName(self.tr("Bar pen"))
        self.bar_symbology_map = BarSymbologyMap()
        self.text_visibility = ptree.Parameter.create(
            name=self.tr("Display category"), type="bool", value=True, default=True
        )
        self.text_color_param = pTypes.ColorParameter(
            name=self.tr("Text color"), value="black", default="black"
        )
        self.define_symbology_button = ptree.Parameter.create(
            name=self.tr("Symbology"), type="action"
        )
        self.define_symbology_button.sigActivated.connect(
            self._display_symbology_dialog
        )

    def set_plot_item(self, plot_item: pg.PlotDataItem) -> None:
        """
        Define plot item containing current assay data

        Args:
            plot_item: pg.PlotDataItem
        """
        super().set_plot_item(plot_item)
        if self.plot_item:
            self.bar_symbology_map = plot_item.symbology_map

    def stack_widget(self, plot_stacked: pg.PlotWidget) -> None:
        """
        Add current plot item to a new plot widget and remove them from current plot widget

        Args:
            plot_stacked (pg.PlotWidget): plot widget where item are added
        """
        super().stack_widget(plot_stacked)
        if self.plot_widget and self.text_bar_graph_item:
            self.plot_widget.removeItem(self.text_bar_graph_item)
            plot_stacked.addItem(self.text_bar_graph_item)

    def unstack_widget(self, plot_stacked: pg.PlotWidget) -> None:
        """
        Remove current plot item from a stacked plot widget and add them to current plot widget

        Args:
            plot_stacked (pg.PlotWidget): plot widget where items are available
        """
        super().unstack_widget(plot_stacked)
        if self.plot_widget and self.text_bar_graph_item:
            plot_stacked.removeItem(self.text_bar_graph_item)
            self.plot_widget.addItem(self.text_bar_graph_item)

    def set_category_text_bar_graph_item(
        self, text_bar_graph_item: TextBarGraphItems
    ) -> None:
        """
        Define plot item containing category text for current assay data

        Args:
            text_bar_graph_item: TextBarGraphItems
        """
        self.text_bar_graph_item = text_bar_graph_item
        if self.text_bar_graph_item:
            # Define current parameters
            self.text_bar_graph_item.setPen(self.pen_params.value())
            self.text_bar_graph_item.setTextColor(self.text_color_param.value())

            # Connection to parameter changes
            self.pen_params.sigValueChanged.connect(
                lambda params, changes: self.text_bar_graph_item.setPen(changes)
            )

            self.text_color_param.sigValueChanged.connect(
                lambda params, changes: self.text_bar_graph_item.setTextColor(changes)
            )

            self.text_visibility.sigValueChanged.connect(
                lambda params, changes: self.text_bar_graph_item.setVisible(changes)
            )

    def add_children_to_root_param(self, params: ptree.Parameter):
        super().add_children_to_root_param(params)
        params.addChild(self.text_visibility)
        params.addChild(self.text_color_param)
        params.addChild(self.define_symbology_button)

    def copy_from_config(self, other) -> None:
        """
        Copy configuration from another configuration.
        If a plot item is associated it will be updated

        Args:
            other: configuration to be copy
        """
        super().copy_from_config(other)
        self.pen_params.setValue(other.pen_params.pen)

        self.bar_symbology_map.merge(other.bar_symbology_map)

        self.text_visibility.setValue(other.text_visibility.value())
        self.text_color_param.setValue(other.text_color_param.value())

        if self.plot_item:
            self.plot_item.set_symbology_map(self.bar_symbology_map)
            self.plot_item.drawPicture()

    def _display_symbology_dialog(self):
        dialog = BarSymbologyDialog()
        dialog.set_symbology_map(self.bar_symbology_map)
        if dialog.exec():
            self.bar_symbology_map = dialog.get_symbology_map()
            if self.plot_item:
                self.plot_item.set_symbology_map(self.bar_symbology_map)
                self.plot_item.drawPicture()
