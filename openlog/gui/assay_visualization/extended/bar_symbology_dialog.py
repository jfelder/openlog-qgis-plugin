import json
import os

from qgis.PyQt import QtCore, uic
from qgis.PyQt.QtGui import QColor
from qgis.PyQt.QtWidgets import (
    QColorDialog,
    QDialog,
    QFileDialog,
    QMessageBox,
    QStyledItemDelegate,
    QWidget,
)

from openlog.gui.assay_visualization.extended.bar_symbology import BarSymbologyMap
from openlog.gui.assay_visualization.extended.bar_symbology_table_model import (
    BarSymbologyMapTableModel,
)


class HtmlColorItemDelegate(QStyledItemDelegate):
    def __init__(self, parent) -> None:
        """
        QStyledItemDelegate for HTML color string definition

        Args:
            parent:
        """
        super().__init__(parent)

    def setEditorData(self, editor: QWidget, index: QtCore.QModelIndex) -> None:
        editor.setCurrentColor(QColor(index.data(QtCore.Qt.DisplayRole)))

    def createEditor(self, parent, option, index) -> QWidget:
        """
        Create a QColorDialog for color definition

        Args:
            parent: QWidget
            option: QStyleOptionViewItem
            index: QModelIndex

        Returns: QDoubleSpinBox

        """
        editor = QColorDialog(parent)
        editor.setOptions(editor.options() | QColorDialog.DontUseNativeDialog)
        return editor

    def setModelData(
        self,
        editor: QWidget,
        model: QtCore.QAbstractItemModel,
        index: QtCore.QModelIndex,
    ) -> None:
        if editor.result() == QDialog.Accepted:
            model.setData(index, editor.selectedColor().name())


class PatternItemDelegate(QStyledItemDelegate):
    def __init__(self, parent) -> None:
        """
        QStyledItemDelegate for HTML color string definition

        Args:
            parent:
        """
        super().__init__(parent)

    def createEditor(self, parent, option, index) -> QWidget:
        """
        Create a QColorDialog for color definition

        Args:
            parent: QWidget
            option: QStyleOptionViewItem
            index: QModelIndex

        Returns: QDoubleSpinBox

        """
        editor = QFileDialog(parent)
        editor.setFileMode(QFileDialog.ExistingFile)
        pattern = BarSymbologyMap().replace_alias_in_pattern(
            str(index.data(QtCore.Qt.DisplayRole))
        )
        editor.selectFile(pattern)
        return editor

    def setModelData(
        self,
        editor: QWidget,
        model: QtCore.QAbstractItemModel,
        index: QtCore.QModelIndex,
    ) -> None:
        """
        Define model data from selected file

        Args:
            editor: QFileDialog
            model: QAbstractItemModel
            index:QModelIndex
        """
        selectedFiles = editor.selectedFiles()
        if len(selectedFiles) == 1:
            pattern = BarSymbologyMap().add_alias_into_pattern(selectedFiles[0])
            model.setData(index, pattern)


class BarSymbologyDialog(QDialog):
    def __init__(self, parent=None) -> None:
        """
        Dialog to display bar symbology

        Args:
            parent: QDialog parent
        """
        super().__init__(parent)
        uic.loadUi(
            os.path.join(os.path.dirname(__file__), "bar_symbology_dialog.ui"), self
        )
        self.setWindowTitle(self.tr("Categorical symbology"))

        self._values_updated = False
        self._bar_symbology_map_model = BarSymbologyMapTableModel()
        self._bar_symbology_map_model.dataChanged.connect(self._model_data_changed)
        self.tableView.setModel(self._bar_symbology_map_model)
        self.tableView.setItemDelegateForColumn(
            self._bar_symbology_map_model.COLOR_COL, HtmlColorItemDelegate(self)
        )
        self.tableView.setItemDelegateForColumn(
            self._bar_symbology_map_model.PATTERN_COL, PatternItemDelegate(self)
        )

        self.save_button.clicked.connect(self.save_symbology)
        self.load_button.clicked.connect(self.load_symbology)

    def accept(self) -> None:
        """
        Check for changes and ask for save

        """
        if self._values_updated:
            ret = QMessageBox.question(
                self,
                self.tr("Save symbology"),
                self.tr(
                    f"You have made changes to your symbology, do you wish to save them to a file?"
                ),
                QMessageBox.Yes | QMessageBox.No | QMessageBox.Cancel,
                QMessageBox.Yes,
            )
            if ret == QMessageBox.No or (
                ret == QMessageBox.Yes and self.save_symbology()
            ):
                super().accept()
        else:
            super().accept()

    def _model_data_changed(self) -> None:
        self._values_updated = True

    def set_symbology_map(self, bar_symbology_map: BarSymbologyMap) -> None:
        """
        Define displayed bar symbology map

        Args:
            bar_symbology_map: (BarSymbologyMap)
        """
        self._bar_symbology_map_model.set_bar_symbology_map(bar_symbology_map)
        self.tableView.resizeColumnsToContents()
        self._values_updated = False

    def get_symbology_map(self) -> BarSymbologyMap:
        """
        Returns displayed bar symbology map

        Returns: (BarSymbologyMap)

        """
        return self._bar_symbology_map_model.get_bar_symbology_map()

    def save_symbology(self) -> bool:
        """
        Save symbology in a JSON file selected by user

        Returns: (bool) True if symbology was saved by user, False otherwise

        """
        saved = False
        filename, filter_use = QFileDialog.getSaveFileName(
            self, "Select file", "bar_symbology.json", "JSON file(*.json)"
        )
        if filename:
            symbology_map = self.get_symbology_map()
            with open(filename, "w") as f:
                f.write(symbology_map.to_json())
            self._values_updated = False
            saved = True

        return saved

    def load_symbology(self) -> None:
        """
        Load symbology from a JSON file selected by user

        """
        filename, filter_use = QFileDialog.getOpenFileName(
            self, "Select file", "", "JSON file(*.json)"
        )
        if filename:
            with open(filename, "r") as f:
                current_symbology_map = self.get_symbology_map()
                current_symbology_map.merge(BarSymbologyMap.from_json(json.load(f)))
                self.set_symbology_map(current_symbology_map)
            self._values_updated = False
