from PyQt5 import QtCore
from PyQt5.QtCore import QVariant
from PyQt5.QtGui import QStandardItemModel, QFont, QColor, QIcon

from openlog.gui.assay_visualization.extended.bar_symbology import BarSymbologyMap, BarSymbology


class BarSymbologyMapTableModel(QStandardItemModel):
    CATEGORY_COL = 0
    PATTERN_COL = 1
    SCALE_COL = 2
    COLOR_COL = 3

    def __init__(self, parent=None) -> None:
        """
        QStandardItemModel for BarSymbologyMap display

        Args:
            parent: QWidget
        """
        super().__init__(parent)
        self.setHorizontalHeaderLabels(
            [self.tr("Category"), self.tr("Pattern"), self.tr("Scale"), self.tr("Color")])

    def flags(self, index: QtCore.QModelIndex) -> QtCore.Qt.ItemFlags:
        """
        Override QStandardItemModel flags to remove edition for category

        Args:
            index: QModelIndex

        Returns: index flags

        """
        flags = super().flags(index)
        if index.column() == self.CATEGORY_COL:
            flags = flags & QtCore.Qt.ItemIsEditable
        return flags

    def data(self, index: QtCore.QModelIndex, role: int = QtCore.Qt.DisplayRole) -> QVariant:
        """
        Override QStandardItemModel data() for :
        - font of category column
        - background color of color column
        - icon of pattern col

        Args:
            index: QModelIndex
            role: Qt role

        Returns: QVariant

        """
        result = super().data(index, role)
        if role == QtCore.Qt.FontRole and index.column() == self.CATEGORY_COL:
            result = QFont()
            result.setBold(True)

        if role == QtCore.Qt.BackgroundRole and index.column() == self.COLOR_COL:
            html_color = str(self.data(index, QtCore.Qt.DisplayRole))
            result = QColor(html_color)

        if role == QtCore.Qt.DecorationRole and index.column() == self.PATTERN_COL:
            pattern = str(self.data(index, QtCore.Qt.DisplayRole))
            pattern = BarSymbologyMap().replace_alias_in_pattern(pattern)
            result = QIcon(pattern)

        return result

    def set_bar_symbology_map(self, bar_symbology_map: BarSymbologyMap) -> None:
        """
        Define displayed bar symbology map

        Args:
            bar_symbology_map: (BarSymbologyMap)
        """
        while self.rowCount():
            self.removeRow(0)

        for category, symb in bar_symbology_map.category_symbology_map.items():
            self._add_category_symbology(category, symb)

    def get_bar_symbology_map(self) -> BarSymbologyMap:
        """
        Returns displayed bar symbology map

        Returns: (BarSymbologyMap)

        """
        res = BarSymbologyMap()

        for i in range(self.rowCount()):
            category = str(self.data(self.index(i, self.CATEGORY_COL)))
            symb = BarSymbology(pattern=str(self.data(self.index(i, self.PATTERN_COL))),
                                scale=self.data(self.index(i, self.SCALE_COL)),
                                color=str(self.data(self.index(i, self.COLOR_COL))),
                                )
            res.category_symbology_map[category] = symb

        return res

    def _add_category_symbology(self, category: str, symbology: BarSymbology) -> None:
        row = self._get_or_create_category(category)
        self.setData(self.index(row, self.PATTERN_COL), symbology.pattern)
        self.setData(self.index(row, self.SCALE_COL), symbology.scale)
        self.setData(self.index(row, self.COLOR_COL), symbology.color)

    def _get_or_create_category(self, category: str) -> int:
        """
        Get or create category

        Args:
            category: category

        Returns: row index

        """
        row = self._get_category_row(category)
        if row == -1:
            self.insertRow(self.rowCount())
            row = self.rowCount() - 1
            self.setData(self.index(row, self.CATEGORY_COL), category)
        return row

    def _get_category_row(self, category: str) -> int:
        """
        Get expected category row index (-1 if expected category not available)

        Args:
            category: (str) expected category

        Returns: expected category row index (-1 if expected category not available)

        """
        column = -1
        for i in range(self.rowCount()):
            val = str(self.data(self.index(i, self.CATEGORY_COL)))
            if val == category:
                column = i
        return column
