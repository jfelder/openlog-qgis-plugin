from typing import Dict, List

import pyqtgraph as pg
from qgis.PyQt import QtCore

from openlog.datamodel.assay.generic_assay import AssayDomainType
from openlog.gui.assay_visualization.assay_inspector_line import AssayInspectorLine
from openlog.gui.assay_visualization.assay_visualization_config import (
    AssayColumnVisualizationConfig,
    AssayVisualizationConfig,
)
from openlog.gui.assay_visualization.stacked.stacked_config import StackedConfiguration


class AssayPlotWidget(pg.PlotWidget):
    def __init__(
        self,
        domain: AssayDomainType,
        parent=None,
        background="default",
        plotItem=None,
        **kargs,
    ):
        """
        pg.PlotWidget override to store domain and extra functions to define limits and display AssayInspectorLine

        Args:
            domain: AssayDomainType
            parent:
            background:
            plotItem:
            **kargs:
        """
        super().__init__(parent, background, plotItem, **kargs)
        self.domain = domain
        self.setBackground("white")

        # Define axis from domain
        if self.domain == AssayDomainType.TIME:
            axis = pg.DateAxisItem()
            self.setAxisItems({"bottom": axis})
            self.getViewBox().setMouseEnabled(y=False)
        else:
            styles = {"color": "black", "font-size": "15px"}
            self.setLabel("left", self.tr("Depth"), "m", **styles)
            self.showAxis("top")

            self.getViewBox().invertY(True)  # Y inversion for depth assay
            self.getViewBox().setMouseEnabled(x=False)

        # Add an inspector line (not attached by default)
        self.inspector = AssayInspectorLine(domain=self.domain)
        self._sync_inspector = None
        self._connection_to_sync_inspector = None
        self._connection_from_sync_inspector = None

        self.assay_config = None
        self.assay_column_config = None
        self.stacked_config = None

        # Disable plot options
        self.disable_plot_option("Average")
        self.disable_plot_option("Alpha")
        self.disable_plot_option("Points")
        self.disable_plot_option("Downsample")

        # Disable some option for transform (only log enable)
        self.plotItem.ctrl.fftCheck.setVisible(False)
        self.plotItem.ctrl.derivativeCheck.setVisible(False)
        self.plotItem.ctrl.phasemapCheck.setVisible(False)
        if self.domain == AssayDomainType.DEPTH:
            self.plotItem.ctrl.logYCheck.setVisible(False)
        else:
            self.plotItem.ctrl.logXCheck.setVisible(False)

    def disable_plot_option(self, option_name: str):
        menu = self.plotItem.getMenu()
        actions = menu.actions()
        for action in actions:
            if action.text() == option_name:
                action.setVisible(False)

    def set_assay_config(self, assay_config: AssayVisualizationConfig) -> None:
        self.assay_config = assay_config
        for key, config in assay_config.column_config.items():
            config.visibility_param.sigValueChanged.connect(self._update_title)

            if hasattr(config, "unit_parameter"):
                config.unit_parameter.sigValueChanged.connect(self._update_title)

        self._update_title()

    def set_assay_column_config(
        self, assay_column_config: AssayColumnVisualizationConfig
    ) -> None:
        self.assay_column_config = assay_column_config
        self.assay_column_config.visibility_param.sigValueChanged.connect(
            self._update_title
        )
        self._update_title()

    def set_stacked_config(self, stacked_config: StackedConfiguration) -> None:
        self.stacked_config = stacked_config
        for config in stacked_config.config_list:
            config.visibility_param.sigValueChanged.connect(self._update_title)
        self._update_title()

    def _update_title(self) -> None:
        if self.assay_column_config:
            collar = self.assay_column_config.hole_display_name
            assay = self.assay_column_config.assay_display_name
            plot_name = f"{collar} / {assay} <br>"
        elif self.assay_config:
            collar = self.assay_config.hole_display_name
            assay = self.assay_config.assay_display_name
            plot_name = f"{collar} / {assay} <br>"
        elif self.stacked_config:
            collars = " ".join(
                set(
                    [
                        c.hole_display_name
                        for c in self.stacked_config.config_list
                        if c.visibility_param.value()
                    ]
                )
            )
            assays = " ".join(
                set(
                    [
                        c.assay_display_name
                        for c in self.stacked_config.config_list
                        if c.visibility_param.value()
                    ]
                )
            )
            plot_name = f"{collars} / {assays} <br>"
        else:
            plot_name = ""

        assay_column_display_name = self._get_assay_column_display_name()
        plot_name += assay_column_display_name
        self.setTitle(plot_name, color="black", size="15px")

        if self.domain == AssayDomainType.TIME:
            styles = {"color": "black", "font-size": "15px"}
            self.setLabel("left", assay_column_display_name, "", **styles)

    def _get_assay_column_display_name(self) -> str:

        if self.assay_column_config is not None:
            assay_column = self.assay_column_config.column
            unit = assay_column.unit

            if hasattr(self.assay_column_config, "unit_parameter"):
                unit = self.assay_column_config.unit_parameter.value()

            assay = self.assay_column_config.assay
            name = (
                assay_column.name
                if assay_column.name != assay.assay_definition.variable
                else ""
            )
            name = f"{name} ({unit})" if unit else name
            plot_name = f" {name}"
        elif self.assay_config:
            config_list = self.assay_config.column_config.values()
            plot_name = self._title_from_assay_column_config_list(config_list)
        elif self.stacked_config:
            config_list = self.stacked_config.config_list
            plot_name = self._title_from_assay_column_config_list(config_list)
        else:
            plot_name = ""
        return plot_name

    @staticmethod
    def _title_from_assay_column_config_list(
        config_list: List[AssayColumnVisualizationConfig],
    ) -> str:
        names = []
        # Add all available column name (if different from assay name, only unit otherwise)
        unit_dict = AssayPlotWidget._create_unit_dict_from_config_list(config_list)
        for unit, unit_names in unit_dict.items():
            name_str = ",".join(unit_names)
            names.append(f"{name_str} ({unit})" if unit else name_str)
        plot_name = " ".join(names)
        return plot_name

    @staticmethod
    def _create_unit_dict_from_config_list(config_list) -> Dict[str, str]:
        unit_dict = {}
        for assay_column_config in config_list:
            if assay_column_config.visibility_param.value():
                assay_column = assay_column_config.column
                assay = assay_column_config.assay
                name = (
                    assay_column.name
                    if assay_column.name != assay.assay_definition.variable
                    else ""
                )
                unit = assay_column.unit

                if hasattr(assay_column_config, "unit_parameter"):
                    unit = assay_column_config.unit_parameter.value()

                if unit in unit_dict:
                    unit_dict[unit].add(name)
                else:
                    unit_dict[unit] = set([name])
        return unit_dict

    def set_limits(self, min_domain_axis: float, max_domain_axis: float) -> None:
        """
        Define limit depending on current domain

        Args:
            min_domain_axis: float
            max_domain_axis: float
        """
        if self.domain == AssayDomainType.TIME:
            self.getViewBox().setLimits(xMin=min_domain_axis, xMax=max_domain_axis)
        else:
            self.getViewBox().setLimits(yMin=min_domain_axis, yMax=max_domain_axis)

        if not self.inspector.valueDefined:
            self.inspector.setPos(
                min_domain_axis + (max_domain_axis - min_domain_axis) / 2.0
            )
            self.inspector.valueDefined = True

    def enable_inspector_line(self, enable: bool) -> None:
        """
        Enable or disable inspector line for this plot

        Args:
            enable: (bool)
        """
        if enable:
            self.inspector.attachToPlotItem(self.getPlotItem())
        else:
            self.inspector.dettach()

    def enable_inspector_sync(
        self, sync_inspector: AssayInspectorLine, enable: bool
    ) -> None:
        """
        Enable inspector line synchronisation

        Args:
            sync_inspector: (AssayInspectorLine) inspector line to be synchronized
            enable: (bool) enable or disable sync
        """
        # Disable current sync
        if self._sync_inspector and self._connection_to_sync_inspector:
            self._sync_inspector.disconnect(self._connection_to_sync_inspector)
            self._sync_inspector = None
            self._connection_to_sync_inspector = None

        if self._connection_from_sync_inspector:
            self.inspector.sigPositionChanged.disconnect(
                self._connection_from_sync_inspector
            )
            self._connection_from_sync_inspector = None

        # Enable if asked
        if enable and sync_inspector != self.inspector:
            self._sync_inspector = sync_inspector
            self._connection_to_sync_inspector = (
                self._sync_inspector.sigPositionChanged.connect(
                    lambda inspector: self.inspector.setPos(inspector.getPos())
                )
            )
            self._connection_from_sync_inspector = (
                self.inspector.sigPositionChanged.connect(
                    lambda inspector: self._sync_inspector.setPos(inspector.getPos())
                )
            )

    def minimumSizeHint(self):
        """
        Return minimum size hint with width calculation from displayed title and axes

        Returns: (QSize) minimum size hint

        """
        res = super().minimumSizeHint()
        if self.plotItem.titleLabel:
            size_title_label = self.plotItem.titleLabel.sizeHint(
                QtCore.Qt.SizeHint.MinimumSize, 0
            )
            res.setWidth(int(size_title_label.width()))
        if self.getAxis("left"):
            size_axis = self.getAxis("left").minimumSize()
            res.setWidth(res.width() + int(size_axis.width()))
            if self.getAxis("left").label:
                res.setHeight(
                    int(self.getAxis("left").label.boundingRect().width() * 0.8)
                )  ## bounding rect is usually an overestimate
        if self.getAxis("right"):
            size_axis = self.getAxis("right").minimumSize()
            res.setWidth(res.width() + int(size_axis.width()))
        if self.getAxis("top"):
            size_axis = self.getAxis("top").minimumSize()
            res.setHeight(res.height() + int(size_axis.height()))
        if self.getAxis("bottom"):
            size_axis = self.getAxis("bottom").minimumSize()
            res.setHeight(res.height() + int(size_axis.height()))
        return res
