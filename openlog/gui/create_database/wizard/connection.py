from PyQt5.QtWidgets import QWizardPage, QLabel, QVBoxLayout

from openlog.datamodel.connection.openlog_connection import Connection
from openlog.gui.connection.postgis_connection_widget import PostgisConnectionWidget
from openlog.toolbelt import PlgTranslator


class ConnectionPageWizard(QWizardPage):
    def __init__(self) -> None:
        """
        QWizard page to define and store database connection for xplordb database creation

        """
        super().__init__()
        self.tr = PlgTranslator().tr
        self.setTitle(self.tr("Database creation"))

        label = QLabel(self.tr("This wizard will help you create an xplordb database"))
        label.setWordWrap(True)

        layout = QVBoxLayout()
        layout.addWidget(label)
        self.connection_widget = PostgisConnectionWidget()
        self.connection_widget.database_creation_use()
        layout.addWidget(self.connection_widget)
        self.setLayout(layout)

        self.conn = None

    def validatePage(self) -> bool:
        """
        Check that connection is valid

        Returns: True if the connection is valid, False otherwise

        """
        return self.connection_widget.test_connection()

    def get_connection(self):
        """
        Get a psycog2 connection

        Returns: psycog2 connection

        """
        return self.connection_widget.get_connection()

    def get_connection_model(self) -> Connection:
        """
        Get connection model from widget

        Returns: connection model

        """
        return self.connection_widget.get_connection_model()
