from datetime import datetime
from typing import List

import numpy as np
import pandas as pd
from qgis.PyQt.QtWidgets import QGridLayout, QMessageBox, QWidget, QWizardPage

from openlog.core import pint_utilities
from openlog.datamodel.assay.generic_assay import (
    AssayColumn,
    AssayDataExtent,
    AssayDefinition,
    AssayDomainType,
    AssaySeriesType,
    GenericAssay,
)
from openlog.datamodel.assay.uncertainty import AssayColumnUncertainty
from openlog.datamodel.connection.openlog_connection import OpenLogConnection
from openlog.gui.import_assay.wizard.assay_selection import AssaySelectionPageWizard
from openlog.gui.utils.column_definition import ColumnDefinition
from openlog.gui.utils.delimited_text_import_widget import DelimitedTextImportWidget
from openlog.toolbelt import PlgTranslator

BASE_SETTINGS_KEY = "/OpenLog/gui/import/assay"


class AssayImportPageWizard(QWizardPage):
    def __init__(
        self,
        parent: QWidget,
        openlog_connection: OpenLogConnection,
        assay_selection_wizard: AssaySelectionPageWizard,
    ) -> None:
        """
        QWizard to define assay import data

        Args:
            assay_selection_wizard : AssaySelectionPageWizard defining assay
            parent : QWidget parent
        """
        super().__init__(parent)
        self._openlog_connection = openlog_connection
        self.tr = PlgTranslator().tr
        self._assay_selection_wizard = assay_selection_wizard

        self.setTitle(self.tr("Downhole data values import"))

        layout = QGridLayout()
        self.dataset_edit = DelimitedTextImportWidget(self, self._openlog_connection)
        layout.addWidget(self.dataset_edit)

        self._HOLE_ID = "Hole ID"
        self._X_COL = "X"
        self._X_END_COL = "X_END"

        self.setLayout(layout)
        self.dataset_edit.restore_settings(BASE_SETTINGS_KEY)

    def validatePage(self) -> bool:
        """
        Validate current page content (return always True since data is optional)

        Returns: True

        """
        valid = True
        self.dataset_edit.save_setting(BASE_SETTINGS_KEY)

        used_category = self.dataset_edit.get_used_category()
        empty_cat = [c for c in used_category if not c]
        if len(empty_cat) != 0:
            valid = False
            QMessageBox.warning(
                self,
                self.tr("No category defined"),
                self.tr(f"Define category name for all categorical columns."),
            )

        return valid

    def initializePage(self) -> None:
        """
        Initialize page before show.

        Update expected column from AssayDefinition

        """
        self.dataset_edit.set_button_layout_visible(
            self._assay_selection_wizard.assay_creation_needed()
        )
        self._set_assay_definition(self._assay_selection_wizard.get_assay_definition())

    def get_assay_definition(self) -> AssayDefinition:
        """
        Returns AssayDefinition from widget

        Returns: AssayDefinition

        """
        assay_definition = self._assay_selection_wizard.get_assay_definition()

        # Add created assay columns
        # Get all expected columns
        expected_columns = self.dataset_edit.get_column_definition()

        # Remove columns already defined in assay definition
        expected_columns = [
            col
            for col in expected_columns
            if col.column
            not in [x.column for x in self._get_columns_definition(assay_definition)]
        ]

        assay_column_uncertainty = self.dataset_edit.get_assay_column_uncertainty()

        for mapping in expected_columns:
            col = mapping.column
            # Define column uncertainty if available
            if col in assay_column_uncertainty:
                uncertainty = assay_column_uncertainty[col]
            else:
                uncertainty = AssayColumnUncertainty()
            assay_definition.columns[col] = AssayColumn(
                name=col,
                series_type=mapping.series_type,
                unit=mapping.unit,
                uncertainty=uncertainty,
                category_name=mapping.category_name,
            )
        return assay_definition

    def get_assays(self) -> List[GenericAssay]:
        """
        Returns GenericAssay with imported data

        Returns: List[GenericAssay]

        """
        result = []
        df = self.dataset_edit.get_dataframe()

        if df is not None:
            # Create GenericAssay for each collar
            for hole_id, hole_id_df in df.groupby(self._HOLE_ID):
                assay = self._create_generic_assay_from_dataframe(hole_id, hole_id_df)
                result.append(assay)

        return result

    def get_used_categories_values(self) -> {str: [str]}:
        """
        Get values imported of each category

        Returns: {str: [str]} list of values for each used category

        """
        return self.dataset_edit.get_used_categories_values()

    def _create_generic_assay_from_dataframe(
        self, hole_id: str, hole_id_df: pd.DataFrame
    ) -> GenericAssay:
        """
        Create GenericAssay from input dataframe

        Args:
            hole_id: (str) hole id
            hole_id_df: (pd.DataFrame)

        Returns: GenericAssay with imported data

        """
        assay_definition = self.get_assay_definition()
        assay = GenericAssay(hole_id=str(hole_id), assay_definition=assay_definition)
        assay.import_x_values = self._import_x_values_from_dataframe(
            assay_definition, hole_id_df
        )
        # Add import y values for assay column
        for column, val in assay_definition.columns.items():
            import_val = hole_id_df.loc[:, column].to_numpy()
            # Data always stored in SI base unit. Convert data before import
            if val.unit:
                import_val = pint_utilities.convert_to_base_unit(val.unit, import_val)
            assay.import_y_values[column] = import_val

        # Add import y values for uncertainty columns
        for column in assay_definition.get_uncertainty_columns():
            assay.import_y_values[column] = hole_id_df.loc[:, column].to_numpy(
                dtype=float
            )
        return assay

    def _import_x_values_from_dataframe(self, assay_definition, hole_id_df) -> np.array:
        """
        Create np.array for GenericAssay import x values

        Args:
            assay_definition: AssayDefinition
            hole_id_df: input pd.DataFrame

        Returns:
            np.array import x values

        """
        x_col_dtype = self._get_x_col_type(assay_definition)
        if assay_definition.data_extent == AssayDataExtent.EXTENDED:
            import_x_values = np.array(
                list(
                    zip(
                        hole_id_df.loc[:, self._X_COL].to_numpy(dtype=x_col_dtype),
                        hole_id_df.loc[:, self._X_END_COL].to_numpy(dtype=x_col_dtype),
                    )
                )
            )
        else:
            import_x_values = hole_id_df.loc[:, self._X_COL].to_numpy(dtype=x_col_dtype)
        return import_x_values

    @staticmethod
    def _get_x_col_type(assay_definition: AssayDefinition) -> type:
        """
        Return expected x column type from AssayDefinition

        Args:
            assay_definition: AssayDefinition

        Returns: expected x column type

        """
        if assay_definition.domain == AssayDomainType.TIME:
            x_col_dtype = datetime
        else:
            x_col_dtype = float
        return x_col_dtype

    def _set_assay_definition(self, assay_definition: AssayDefinition) -> None:
        """
        Define DelimitedTextImportWidget options from AssayDefinition

        Args:
            assay_definition: AssayDefinition
        """
        self._define_col_names(assay_definition)
        expected_columns = self._get_columns_definition(assay_definition)
        self.dataset_edit.set_column_definition(expected_columns)

    def _define_col_names(self, assay_definition: AssayDefinition) -> None:
        """
        Define default column name from AssayDefinition

        Args:
            assay_definition: AssayDefinition
        """
        if assay_definition.domain == AssayDomainType.TIME:
            self._X_COL = "Time"
        else:
            self._X_COL = "Depth"

        if assay_definition.data_extent == AssayDataExtent.EXTENDED:
            self._X_END_COL = self._X_COL + " max."
            self._X_COL = self._X_COL + " min."

    def _get_columns_definition(
        self, assay_definition: AssayDefinition
    ) -> [ColumnDefinition]:
        """
        Get expected columns from AssayDefinition

        Args:
            assay_definition:

        Returns: expected columns

        """
        x_unit = "m" if assay_definition.domain == AssayDomainType.DEPTH else "datetime"
        x_series_type = (
            AssaySeriesType.NUMERICAL
            if assay_definition.domain == AssayDomainType.DEPTH
            else AssaySeriesType.DATETIME
        )
        expected_columns = [
            ColumnDefinition(
                column=self._HOLE_ID, fixed=True, series_type=AssaySeriesType.NOMINAL
            ),
            ColumnDefinition(
                column=self._X_COL,
                unit=x_unit,
                fixed=True,
                series_type=x_series_type,
            ),
        ]
        if assay_definition.data_extent == AssayDataExtent.EXTENDED:
            expected_columns.append(
                ColumnDefinition(
                    column=self._X_END_COL,
                    unit=x_unit,
                    fixed=True,
                    series_type=x_series_type,
                )
            )
        for key, column in assay_definition.columns.items():
            expected_columns.append(
                ColumnDefinition(
                    column=key,
                    fixed=True,
                    unit=column.unit,
                    series_type=column.series_type,
                    category_name=column.category_name,
                )
            )
        return expected_columns
