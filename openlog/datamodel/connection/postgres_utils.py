from sqlalchemy import create_engine
from sqlalchemy.engine import URL, Engine
from sqlalchemy.orm import Session, sessionmaker

from openlog.datamodel.connection.openlog_connection import Connection


def create_session(connection: Connection) -> (Session, Engine):
    """
    Create a sqlalchemy session for postgres database

    Returns: sqlachemy session

    """
    connection_url = URL.create(
        "postgresql+psycopg2",
        username=connection.user,
        password=connection.password,
        host=connection.host,
        port=connection.port,
        database=connection.database,
    )

    engine = create_engine(connection_url)

    postgres_session = sessionmaker(bind=engine)
    session = postgres_session()

    return session, engine
