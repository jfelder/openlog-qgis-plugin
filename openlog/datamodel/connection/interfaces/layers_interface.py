from pathlib import Path
from typing import List

from qgis.core import QgsVectorLayer

from openlog.__about__ import DIR_PLUGIN_ROOT


class LayersInterface:
    """
    Interface for layers use.

    Get collar or collar trace layer :

    - :meth:`get_collar_layer`
    - :meth:`get_collar_trace_layer`

    Define path for default style for layers:

    - :meth:`get_collar_layer_style_file`
    - :meth:`get_collar_trace_layer_style_file`

    Get collar layer name:

    - :meth:`get_collar_layer_name`
    - :meth:`get_collar_trace_layer_name`

    Get selected collar :

    - :meth:`get_selected_collar_from_layer`

    By default, all functions are not implemented and raises :class:`InvalidInterface` exception.

    """

    class InvalidInterface(Exception):
        pass

    def __init__(self):
        # Store created collar layers, so we can access selected feature in widget only with OpenLogConnection interface
        self.collar_layer = None
        self.collar_trace_layer = None

    def get_collar_layer(self) -> QgsVectorLayer:
        """
        Return collar QgsVectorLayer

        Returns:
            QgsVectorLayer: raises :class:`InvalidInterface` if not implemented
        """
        raise LayersInterface.InvalidInterface()

    def get_collar_trace_layer(self) -> QgsVectorLayer:
        """
        Return collar trace QgsVectorLayer

        Returns:
            QgsVectorLayer: raises :class:`InvalidInterface` if not implemented
        """
        raise LayersInterface.InvalidInterface()

    def get_collar_layer_style_file(self) -> Path:
        """
        Get QGIS file style for collar layer

        Returns:
            Path: path to QGIS style file
        """
        return DIR_PLUGIN_ROOT / "resources" / "styles" / "collar.qml"

    def get_collar_trace_layer_style_file(self) -> Path:
        """
        Get QGIS file style for collar trace layer

        Returns:
            Path: path to QGIS style file
        """
        return DIR_PLUGIN_ROOT / "resources" / "styles" / "collar_trace.qml"

    def get_collar_layer_name(self) -> str:
        """
        Get collar layer name

        Returns:
            str: collar layer name
        """
        return self.tr("Collar")

    def get_collar_trace_layer_name(self) -> str:
        """
        Get collar trace layer name

        Returns:
            str: collar trace layer name
        """
        return self.tr("Trace")

    def get_selected_collar_from_layer(self) -> List[str]:
        """
        Get selected collar id from QGIS layer

        Returns:
            List[str]: selected collar id
        """
        collar_layer = self.get_collar_layer()
        if collar_layer is not None:
            res = [f["hole_id"] for f in collar_layer.selectedFeatures()]
        else:
            res = []
        return res
