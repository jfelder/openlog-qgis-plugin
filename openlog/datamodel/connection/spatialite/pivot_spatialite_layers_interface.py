from qgis.core import QgsVectorLayer

from openlog.datamodel.connection.interfaces.layers_interface import LayersInterface
from openlog.datamodel.connection.openlog_connection import Connection
from openlog.datamodel.connection.spatialite.spatialite_connection import (
    SpatialiteConnection,
)
from openlog.toolbelt import PlgTranslator


class PivotSpatialiteLayersInterface(LayersInterface):
    def __init__(
        self, connection: Connection, spatialite_connection: SpatialiteConnection
    ):
        """
        Implementation of LayersInterface with a pivot spatialite connection

        Methods are updated to :
        - set layers read only
        - add database used in connection in layers name

        Args:
            connection: Connection used
            spatialite_connection: SpatialiteConnection used for pivot
        """
        super().__init__()
        self._connection = connection
        self._spatialite_connection = spatialite_connection
        self.tr = PlgTranslator().tr

    def get_collar_layer(self) -> QgsVectorLayer:
        """
        Return collar QgsVectorLayer

        In acquire we use pivot spatialite database to store collar geometry

        """
        if self.collar_layer is None:
            self.collar_layer = (
                self._spatialite_connection.get_layers_iface().get_collar_layer()
            )
            self.collar_layer.setReadOnly(True)
            self.collar_layer.setName(self.get_collar_layer_name())
        return self.collar_layer

    def get_collar_layer_name(self) -> str:
        """
        Get collar layer name

        Returns: (str) collar layer name

        """
        return self.tr("Collar - [{0}] (pivot spatialite)").format(
            self._connection.database
        )

    def get_collar_trace_layer(self) -> QgsVectorLayer:
        """
        Return collar trace QgsVectorLayer

        In acquire we use pivot spatialite database to store collar trace geometry

        """
        if self.collar_trace_layer is None:
            self.collar_trace_layer = (
                self._spatialite_connection.get_layers_iface().get_collar_trace_layer()
            )
            self.collar_trace_layer.setReadOnly(True)
            self.collar_trace_layer.setName(self.get_collar_trace_layer_name())
        return self.collar_trace_layer

    def get_collar_trace_layer_name(self) -> str:
        """
        Get collar trace layer name

        Returns: (str) collar trace layer name

        """
        return self.tr("Trace - [{0}] (pivot spatialite)").format(
            self._connection.database
        )
