#! python3  # noqa: E265

# ----------------------------------------------------------
# Copyright (C) 2015 Martin Dobias
# ----------------------------------------------------------
# Licensed under the terms of GNU GPL 2
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
# --------------------------------------------------------------------


"""
You will find here a description of all openlog package :

- ``openlog.core`` : contains all classes used to implement algorithms (desurveying, lithology interpolation)
- ``openlog.datamodel`` : contains all classes for openlog datamodel (assays, openlog connection)
- ``openlog.gui`` : contains all classes for openlog gui
- ``openlog.processing`` : contains all classes for openlog QgsProcessing implementation
- ``openlog.toolbelt`` : contains all classes for openlog that are not related to openlog and generic for others QGIS plugin

"""


def classFactory(iface):
    """Load openlog plugin.

    Needed by QGIS to load plugin.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    from .plugin_main import OpenlogPlugin

    return OpenlogPlugin(iface)
