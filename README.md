# OpenLog - QGIS Plugin

Development of an Open Source drillhole visualization and editing module in QGIS Mining industry professionals are in natural need of solutions for drillhole visualization, management, and edition.

Although several free or Open Source solutions exist on the market to partially address the need, none has propagated into common use.

In partnership with a team of mining industry leaders, Oslandia intends to develop a high performance drillhole visualization QGIS module supporting 3D, cross-section, and log views.

Consult [the project documentation](https://geolandia.gitlab.io/openlog/openlog-qgis-plugin/index.html) for further details or [activate the relevant notifications](https://vimeo.com/732029377) to stay in tune with the OpenLog crew.

----

[![Code style: black](https://img.shields.io/badge/code%20style-black-000000.svg)](https://github.com/psf/black)
[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
[![pre-commit](https://img.shields.io/badge/pre--commit-enabled-brightgreen?logo=pre-commit&logoColor=white)](https://github.com/pre-commit/pre-commit)

[![flake8](https://img.shields.io/badge/linter-flake8-green)](https://flake8.pycqa.org/)

