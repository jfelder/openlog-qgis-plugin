# User manual

## Database creation

OpenLog provides its own borehole data storage library called [xplordb](https://gitlab.com/geolandia/openlog/xplordb).  
`xplordb` meets [GeoSciML](https://www.ogc.org/standards/geosciml) standards and relies on `PostgreSQL/Postgis` or `spatialite` for its backend component.

<div style="padding:52.92% 0 0 0;position:relative;"><iframe src="https://player.vimeo.com/video/733919902?h=ec4d0bcd08&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="How to set up a database in OpenLog"></iframe></div><script src="https://player.vimeo.com/api/player.js"></script>

### Spatialite - standalone

Follow the instructions in the [quick start](./quick_start.md/#database-creation) database creation section.

### PostgreSQL - xplordb

To create a new `xplordb` database and provided that you have access to an active PostgreSQL + Postgis server:

   1. go to `OpenLog` menu > `Database` > `Create new xplordb database`

      ```{image} images/create_new_xplordb_01_menu.png
      :alt: create_new_xplordb_01_menu
      :class: bg-primary
      :width: 400px
      :align: center
      ```

   2. input host and port details to connect to the PostgreSQL backend database

      ```{image} images/create_new_xplordb_02_connection.png
      :alt: create_new_xplordb_02_connection
      :class: bg-primary
      :width: 400px
      :align: center
      ```

      > **_NOTE:_**  database creation is only available to PostgreSQL users with sufficient privileges

   3. review authentication parameters in the `Basic` tab
   4. verify authentication parameters by using the `Test connection button` then click `Next` button.
   5. input a database name and set remaining parameters as desired

      ```{image} images/create_new_xplordb_03_xplordb_parameters.png
      :alt: create_new_xplordb_03_xplordb_parameters
      :class: bg-primary
      :width: 400px
      :align: center
      ```

      > **_NOTE:_** the `admin` user is mandatory and should be an `xdb_admin`

   6. click `Finish`
   7. connect to the newly created database

## Database connection

> **_NOTE: _** OpenLog integrates to multiple 3rd party databases but it is more efficient to work with native `xplordb` or `spatialite` databases.

### To spatialite

1. go to `OpenLog` menu > `Database` > `Connect to spatialite database`
2. browse to the database file

### To PostgreSQL

1. go to `OpenLog` menu > `Database` > `Connect to xplordb database`
2. input the host and port details of the PostgreSQL backend database
3. input the `xplordb` database name
4. go to the Basic tab and input the `xplordb` `admin` credentials

### To Geotic

1. go to `OpenLog` menu > `Database` > `Connect to geotic database`
2. input the host and port details of the MSSQL backend database
3. input the `geotic` database name
4. input the `geotic` user login and password

### To BD Geo

1. got to `OpenLog` menu > `Database` > `Connect to BD Geo database`
2. input the host and port details of the PostgreSQL backend database
3. input `BD Geo` database name
4. input the `BD Geo` user login and password

## Importing data

In addition to its database connection infrastructure, OpenLog provides a csv import wizard for drillhole data.

### Import collar, survey, lithology csv files together

Once connected to an `xplordb` or `spatialite` database you may import 3 files csv data:

1. go to `OpenLog` menu > `Database` > `Import collar, survey, lithology csv`

   ```{image} images/import_data_01_menu.png
   :alt: import_data_01_menu
   :class: bg-primary
   :width: 400px
   :align: center
   ```

2. select or create the person doing the data import job

   ```{image} images/import_data_02_person.png
   :alt: import_data_02_person
   :class: bg-primary
   :width: 300px
   :align: center
   ```

3. select or create the dataset where the data will be imported.

   ```{image} images/import_data_03_dataset.png
   :alt: import_data_03_dataset
   :class: bg-primary
   :width: 300px
   :align: center
   ```

   Click `Next`

4. browse to a **collar** csv file, parameterize the importer, then click `Next`

   In the title section are the following parameters:
      - `File name`: the path to the collar csv file
      - `CRS`: an EPSG reference to the Coordinate Reference System to be used
      - `DTM` (optional): a Digital Terrain Model from which to extract collar elevations
      - `Date format` (optional): a Date, Time, and Time zone parsing templater
      - `Current time` (read-only): an example of the expected data according to `Date format`
  
   In the `File Format` section are the following parameters:
      - `CSV (comma separated values)` or `Custom delimiters` (mandatory): define the delimiter characters and text qualifiers for the purpose of parsing the csv file into a structured table

   In the `Column definition` section is a table of 7 columns and 4 rows:
      - The `Column` row references the column titles
      - The `Map` row presents a drop-down list, the list is comprised of the colum titles found in the csv file according to the user-defined `File Format` rules 
      - The `Unit` row specifies the physical unit of measurement
      - The `Type` row specifies the data type, the available data types are `Nominal`, `Numerical`, `Datetime`, and `Categorical`
      - The `HoleID` column maps unique collar identifiers
      - The `Easting` column maps collar x coordinates in metres
      - The `Northing` column maps collar y coordinates in metres
      - The `Elevation` column (optional) maps collar z coordinates in metres ASML
      - The `End Of Hole` column (optional) maps collar total drilled length in metres
      - The `Survey Date` column (optional) maps collar the date/time initial survey

   In the `Sample Data` section is a preview table where the `File Format` parsing rules are applied to the csv file

   In the `Imported data` section is a preview table where the `Column definition` mapping rules are applied to the parsed csv file. This represents how the data will be stored in the database.

   ```{image} images/import_collar_02_menu.png
   :alt: import_collar_02_menu
   :class: bg-primary
   :width: 500px
   :align: center
   ```

   Click `Next`

5. browse to a **survey** csv file, parameterize the importer, then click `Next`

   In the `Import options` section are the following parameters:
      - `Invert Dips` checkbox, OpenLog assumes negative dips down and checking the box will inverse this convention
      - `File name` references the path to the survey csv file
  
   In the `File Format` section are the following parameters:
      - `CSV (comma separated values)` or `Custom delimiters` (mandatory): define the delimiter characters and text qualifiers for the purpose of parsing the csv file into a structured table

   In the `Column definition` section is a table of 5 columns and 4 rows:
      - The `Column` row references the column titles
      - The `Map` row presents a drop-down list, the list is comprised of the colum titles found in the csv file according to the user-defined `File Format` rules 
      - The `Unit` row specifies the physical unit of measurement
      - The `Type` row specifies the data type, the available data types are `Nominal`, `Numerical`, `Datetime`, and `Categorical`
      - The `HoleID` column maps unique collar identifiers
      - The `Dip` column maps survey inclination in decimal degrees from +90° to -90° with 0° defined has the horizontal plane
      - The `Azimuth` column maps survey direction in decimal degrees from +0° to +359° clockwise with 0° defined has the grid North  
      - The `Length` column maps survey drilled lengths in metres

   In the `Sample Data` section is a preview table where the `File Format` parsing rules are applied to the csv file

   In the `Imported data` section is a preview table where the `Column definition` mapping rules are applied to the parsed csv file. This represents how the data will be stored in the database.

   ```{image} images/import_data_05_survey.png
   :alt: import_data_05_survey
   :class: bg-primary
   :width: 500px
   :align: center
   ```

   Click `Next`

6. browse to a **lithology** csv file, parameterize the importer, then click `Next`

   In the `Import options` section are the following parameters:
      - `Gap resolution` specifies how lithological interval gaps are handled:
        - `accept` surveys from drillholes that present gaps are retained
        - `reject` surveys from drillholes that present gaps will are removed
        - `forward expansion` surveys from drillholes that present gaps will have their corresponding overlying lithological intervals expanded to fill the gap
        - `backward expansion` surveys from drillholes that present gaps will have their corresponding underlying lithological intervals expanded to fill the gap
        - `nearest neighbor` surveys from drillholes that present gaps will have their corresponding overlying as well as underlying lithological intervals expanded to fill the gap to the median point
      - `Overlap resolution` specifies how lithological interval overlaps are handled:
        - `reject` surveys from drillholes that present overlaps will not be imported
        - `forward expansion` surveys from drillholes that present overlaps will have them removed, then have the corresponding overlying lithological intervals expanded to fill the newly created gap
        - `backward expansion` surveys from drillholes that present overlaps will have them removed, then have the corresponding underlying lithological intervals expanded to fill the newly created gap
        - `nearest neighbor` surveys from drillholes that present overlaps will have them removed, then have the corresponding overlying as well as underlying lithological intervals expanded to fill the newly created gap to the median point
      - `File name` references the path to the lithology csv file
  
   In the `File Format` section are the following parameters:
      - `CSV (comma separated values)` or `Custom delimiters` (mandatory): define the delimiter characters and text qualifiers for the purpose of parsing the csv file into a structured table

   In the `Column definition` section is a table of 5 columns and 4 rows:
      - The `Column` row references the column titles
      - The `Map` row presents a drop-down list, the list is comprised of the colum titles found in the csv file according to the user-defined `File Format` rules 
      - The `Unit` row specifies the physical unit of measurement
      - The `Type` row specifies the data type, the available data types are `Nominal`, `Numerical`, `Datetime`, and `Categorical`
      - The `HoleID` column maps unique collar identifiers
      - The `LithCode` column maps lithological interval identifiers
      - The `From_m` column maps the top of each lithological interval
      - The `To_m` column maps the bottom of each lithological interval

   In the `Categories` section is a table of 3 columns and 1 row:
      - The `Column` column maps all categorical data as defined in the `Column definition` section
      - The `Category` column maps to a dictionary of allowed values
      - The `Validation` column presents a drop-down list, the list offers three filter options: `append` will add any unrecognized category from the csv file to the dictionary and therefore import all entries; `restrict` will eliminate single entries with unrecognized categories; `remove` will eliminate entries with unrecognized categories and all entries associated to the same `HoleID`

   ```{image} images/import_data_categories.png
   :alt: import_data_categories
   :class: bg-primary
   :width: 200px
   :align: center
   ```

   In the `Sample Data` section is a preview table where the `File Format` parsing rules are applied to the csv file

   In the `Imported data` section is a preview table where the `Column definition` mapping rules are applied to the parsed csv file. This represents how the data will be stored in the database.

      ```{image} images/import_data_06_litho.png
   :alt: import_data_06_litho
   :class: bg-primary
   :width: 500px
   :align: center
   ```

7. review the summary of what **is to be be** imported into the database and click `Finish`
8. (optional) right-click the newly created `collar` layer and select `Zoom to layer` to display your data.

### Import collar csv files

1. go to `OpenLog` menu > `Database` > `Import collar csv`

   ```{image} images/import_collar_01_menu.png
   :alt: import_collar_01_menu
   :class: bg-primary
   :width: 400px
   :align: center
   ```

2. follow steps 2 to 4 of the [Import collar, survey, lithology csv files](#Import-collar,-survey,-lithology-csv-files) section
3. review the summary of what **is to be be** imported into the database and click `Finish`

### Import survey csv files

1. go to `OpenLog` menu > `Database` > `Import survey csv`

   ```{image} images/import_survey_01_menu.png
   :alt: import_survey_01_menu
   :class: bg-primary
   :width: 400px
   :align: center
   ```

2. follow steps 2, 3, and 5 of the [Import collar, survey, lithology csv files](#Import-collar,-survey,-lithology-csv-files) section
3. review the summary of what **is to be be** imported into the database and click `Finish`

### Import lithology csv files

1. go to `OpenLog` menu > `Database` > `Import lithology csv`

   ```{image} images/import_lithology_01_menu.png
   :alt: import_lithology_01_menu
   :class: bg-primary
   :width: 400px
   :align: center
   ```

2. follow steps 2, 3, and 6 of the [Import collar, survey, lithology csv files](#Import-collar,-survey,-lithology-csv-files) section
3. review the summary of what **is to be be** imported into the database and click `Finish`

### Import downhole data csv files

1. go to `OpenLog` menu > `Database` > `Import downhole data csv`

   ```{image} images/import_assay_01_menu.png
   :alt: import_assay_01_menu
   :class: bg-primary
   :width: 400px
   :align: center
   ```

2. Either pick an existing downhole data series from the `Available downhole data` drop down menu or create a new one under `Create downhole data`.  
    When chosing the latter, the following parameters are made available:
      - The `Variable` parameter refers to the new downhole data series name as defined by the user
      - the `Domain` parameter refers to the authorized input set of the new downhole data series, it offers the `depth` option for measurements taken along the hole path and the `time` option for measurements taken over time at a single set depth
      - The `Extent` parameters refers to the granularity of the input set, it offers the `discrete` option for point measurements and the `extended` option for interval measurements

   ```{image} images/import_data_02_assay.png
   :alt: import_data_02_assay
   :class: bg-primary
   :width: 400px
   :align: center
   ```

   click `Next`

3. browse to a **downhole data** csv file, parameterize the importer, then click `Next`

   In the title section are the following parameters:
      - `File name`: the path to the csv file
      - `Date format` (optional): a Date, Time, and Time zone parsing templater
      - `Current time` (read-only): an example of the expected data according to `Date format`lithological intervals expanded to fill the newly created gap to the median point
      - `File name` references the path the csv file
  
   In the `File Format` section are the following parameters:
      - `CSV (comma separated values)` or `Custom delimiters` (mandatory): define the delimiter characters and text qualifiers for the purpose of parsing the csv file into a structured table

   In the `Column definition` section is a table of 2 to 3 columns and 4 rows:
      - The `Column` row references the column titles
      - The `Map` row presents a drop-down list, the list is comprised of the colum titles found in the csv file according to the user-defined `File Format` rules
      - The `Unit` row specifies the physical unit of measurement
      - The `Type` row specifies the data type, the available data types are `Nominal`, `Numerical`, `Datetime`, and `Categorical`

         ```{image} images/import_data_types.png
         :alt: import_data_types
         :class: bg-primary
         :width: 250px
         :align: center
         ```
  
      - The `HoleID` column maps unique collar identifiers

      When working with a `discrete` input set, the `Depth` column maps measurement depths.

      When working with a `extended` input set, the `From_m` column maps the top of each interval while the `To_m` column maps the bottom of each interval.

   Either way, you will be able to add/remove any number of data columns by clicking the green `+` icon or remove them with the red `x` icon. The `Column`, `Map`, `Unit`, and `Type` parameters will have to be set for each newly added column.

   > **If** you have added at least one column with the `Type` set to `Categorical`, the `Categories` section will display a table of 3 columns and as many rows as the number there are `Categorical` variables:
      - The `Column` column maps all categorical data as defined in the `Column definition` section
      - The `Category` column maps to a dictionary of allowed values
      - The `Validation` column presents a drop-down list, the list offers three filter options: `append` will add any unrecognized category from the csv file to the dictionary and therefore import all entries; `restrict` will eliminate single entries with unrecognized categories; `remove` will eliminate entries with unrecognized categories and all entries associated to the same `HoleID`

   ```{image} images/import_data_categories.png
   :alt: import_data_categories
   :class: bg-primary
   :width: 250px
   :align: center
   ```

   > **If** you have added at least one column with the `Type` set to `Numerical`, the `Uncertainty` section will display a table of 2 to 5 columns (based on the number of values used to describe uncertainty) and as many rows as the number there are `Numerical` variables.
     - The `Column` column maps all numerical data as defined in the `Column definition` section

     When working with `Interval` uncertainty, the `Wide interval` column maps the extent of an error bar relative to the measured value, the underlying distribution is assumed symmetrical

     When working with `Boundary pair` uncertainty, the `Max wide interval` and `Min wide interval` columns map the highest and lowest values of an error bar.

     When working with `Boundary quad` uncertainty, the `Max wide interval` and `Min wide interval` columns map the highest and lowest values of the error bar component of a box plot while the `Max narrow interval` and `Min narrow interval`  columns map the highest and lowest values of the box component of a box plot.

   ```{image} images/boxplot.png
   :alt: boxplot
   :class: bg-primary
   :width: 400px
   :align: center
   ```

   In the `Sample Data` section is a preview table where the `File Format` parsing rules are applied to the csv file

   In the `Imported data` section is a preview table where the `Column definition` mapping rules are applied to the parsed csv file. This represents how the data will be stored in the database.

   ```{image} images/import_data_03_assay.png
   :alt: import_data_03_assay
   :class: bg-primary
   :width: 400px
   :align: center
   ```

   click `Next`

4. verify the database table creation parameters, ensure that no blank characters are present in any `Database column` or `Table Name` entries

   ```{image} images/import_data_04_assay.png
   :alt: import_data_04_assay
   :class: bg-primary
   :width: 400px
   :align: center
   ```

   click `Next`

5. review the summary of what is to be be imported into the database and click 'Finish'

   ```{image} images/import_data_05_assay.png
   :alt: import_data_05_assay
   :class: bg-primary
   :width: 400px
   :align: center
   ```

## Adding collars

1. go to `OpenLog` menu > `Add collar`

   ```{image} images/add_collar_01_menu.png
   :alt: add_collar_01_menu
   :class: bg-primary
   :width: 250px
   :align: center
   ```

2. select or create a `Person` and `Dataset`
3. in the `Collar settings` section, set the general parameters:

   - `Index prefix` prepends a string to each collar name
   - `Index base` defines the total digit count of collar numbers
   - `Index start` appends a collar number number to each collar name
   - `DTM` points to an existing raster layer that will be used to retrieve collar elevation

4. tick the `Grid mode` checkbox if required

### Point & click mode

If the `Grid mode` box is left unchecked, the user is generate new collar via point and click action directly onto the canvas to add new collar entries into an editable table of 6 columns:

- An untitled column for rank
- The `HoleID` column displays unique collar identifiers generated as per the `Index prefix`, `Index base`, and `Index start` parameters
- The `Easting` column displays the X coordinates retrieved on click from the canvas in metres
- The `Northing` column displays the Y coordinates retrieved on click from the canvas in metres
- The `Elevation` column displays the Z coordinates retrieved on click from the `DTM` in metres, defaults to 0 is no elevation surface is present
- The `EOH` column displays an optional, user-input, record of the total measured length of the drillhole

   ```{image} images/add_collar_02_menu.png
   :alt: add_collar_02_menu
   :class: bg-primary
   :width: 400px
   :align: center
   ```

Click `OK` to validate the changes and push the new collars to the database.

### Grid mode

If the `Grid mode` box is checked, the user is able to generate a grid of points of varying orientation and spacing. The relevant parameters are:

- `Number` and `Spacing` represent two alternative means of defining the cardinality of the grid, either through a `Columns` number vs `Rows` number or via `Horizontal` intervals vs `Vertical` intervals
- The `Azimuth` defines the orientation of the grid relative to the grid North

The points may then be added to an editable collar table of columns by clicking `Add`:

- An untitled column for rank
- The `HoleID` column displays unique collar identifiers generated as per the `Index prefix`, `Index base`, and `Index start` parameters
- The `Easting` column displays the X coordinates retrieved on click from the canvas in metres
- The `Northing` column displays the Y coordinates retrieved on click from the canvas in metres
- The `Elevation` column displays the Z coordinates retrieved on click from the `DTM` in metres, defaults to 0 is no elevation surface is present
- The `EOH` column displays an optional, user-input, record of the total measured length of the drillhole

   ```{image} images/add_collar_03_menu.png
   :alt: add_collar_03_menu
   :class: bg-primary
   :width: 400px
   :align: center
   ```

Click `OK` to validate the changes and push the new collars to the database.

## Desurveying

OpenLog makes use of the minimum curvature interpolator for its desurveying calculations.

In order to avoid potentially lengthy processing times, OpenLog does not desurvey drillholes on import or creation by default.

1. Select any number of collars from the collar layer
2. go to `OpenLog` menu > `Desurvey holes` or right click into the canvas and click `Desurvey holes`
  
   ```{image} images/desurvey_01_menu.png
   :alt: desurvey_01_menu
   :class: bg-primary
   :width: 250px
   :align: center
   ```

3. the orthogonal projection of the drillhole tails/traces will be made visible over the canvas

   ```{image} images/desurvey_02_results.png
   :alt: desurvey_02_results
   :class: bg-primary
   :width: 300px
   :align: center
   ```

## Editing surveys

OpenLog includes basic survey edition capabilities.

1. Select any number of collars from the collar layer
2. go to `OpenLog` menu > `Edit surveys` or right click into the canvas and click `Edit surveys`

   ```{image} images/survey_edit_01_menu.png
   :alt: survey_edit_01_menu
   :class: bg-primary
   :width: 250px
   :align: center
   ```

3. select or create a `Person` and `Dataset`
4. in the case of multiple collar selections, a `Collars` drop-down list of their HoleIDs will appear where each may be selected individually

   ```{image} images/survey_edit_03_table.png
   :alt: survey_edit_03_table
   :class: bg-primary
   :width: 400px
   :align: center
   ```

5. based on the `Collars` selection, relevant existing survey entries will populate a 3 column table in the `Survey` section.

   - The `Depth` column defines the drilled length at which the survey entry was measured
   - The `Dip` column maps survey inclination in decimal degrees from +90° to -90° with 0° defined has the horizontal plane
   - The `Azimuth` column maps survey direction in decimal degrees from +0° to +359° clockwise with 0° defined has the grid North  

   New survey entries may be added or deleted by clicking the green `+` icon or the red `x` icon, respectively.

   ```{image} images/survey_edit_02_table.png
   :alt: survey_edit_02_table
   :class: bg-primary
   :width: 400px
   :align: center
   ```

6. click `OK` to push the changes to the database

## Visualizing data

### Depth domain downhole data visualization

1. Select any number of collars from the collar layer
2. go to `OpenLog` menu > `Display depth data` or right click into the canvas and click `Display depth data`

   ```{image} images/depth_viz_01_menu.png
   :alt: depth_viz_01_menu
   :class: bg-primary
   :width: 250px
   :align: center
   ```

3. click the `Add` icon in the newly displayed data visualization panel

   ```{image} images/depth_viz_02_menu.png
   :alt: depth_viz_02_menu
   :class: bg-primary
   :width: 250px
   :align: center
   ```

4. select any number of available assay from the drop-down list and click `OK`

   ```{image} images/depth_viz_03_menu.png
   :alt: depth_viz_03_menu
   :class: bg-primary
   :width: 250px
   :align: center
   ```

  A two-pronged selection tree will then populate itself with the relevant collars and downhole data references at the left side of the visualization panel.  

  The top tree first references downhole data series as selected step 4, each downhole data series then encapsulates a list of collars as selected step 1.  
  The bottom tree first references collars as selected step 1, each collar then encapsulates a list of downhole data series as selected step 4.  

  Both trees map the same underlying data structure and serve as two effectively equivalent pathways to interact with the display and symbology options described below.

   ```{image} images/depth_viz_04_tree.png
   :alt: depth_viz_04_tree
   :class: bg-primary
   :width: 250px
   :align: center
   ```

#### Controls

##### General

Graphs may be reordered using the menu bar at the top of the panel:

- `Sort by source` rearranges the graphs per the order of their data series in the top tree
- `Sort by collar` rearranges the graphs per the order of their collar in the botttom tree
- `Transpose` rearranges the graphs to be stacked horizontally. In this configuration, the zoom level will be shared among all graphs

   ```{image} images/depth_viz_06_sort.png
   :alt: depth_viz_06_sort
   :class: bg-primary
   :width: 200px
   :align: center
   ```

##### Contextual

The standard PyQtGraph contextual menu may be summoned with a right click into the display area. Note that, in this section, only the entries that are relevant to OpenLog are being documented.

   ```{image} images/depth_viz_07_numerical_symbology_8.png
   :alt: depth_viz_07_numerical_symbology_8
   :class: bg-primary
   :width: 300px
   :align: center
   ```

- The `X-axis` and `Y-axis` entries are used to define the numerical extent of the graph either manually, as an upper/lower boundary pair, or automatically, as a percentage.
- The `Plot Options` entry offers access to the following items:
  - The `Transforms` item toggles the X and Y scaling to decimal log
  - The `Alpha` item controls the transparency of the plot
  - The `Grid` item toggles the grid views for the X and Y axes
- The `Export` entry offers access the following file export options: `CSV`, `Image`, `Matplotlib`, and `SVG`


##### Interactive

Scrolling up and down a graph can be achieved by hovering the cursor over the display area with the following inputs:

- Ctrl + M3 up/down
- Left click drag up/down

Zoom in/out is achieved with M3 up/down

#### Discrete numerical data

In the case of discrete numerical data, a series of vertically stacked and fully zoomed out line graphs will fill the right side of the visualization panel.

   ```{image} images/depth_viz_05_numerical.png
   :alt: depth_viz_05_numerical
   :class: bg-primary
   :width: 750px
   :align: center
   ```

##### Symbology

- The `Name (Unit)` category toggles the rendering of the graph and its exact wording is derived from the downhole data series
  - The `Bar chart/Line chart` toggles the graph display between a triangulated line chart and a median point centered bar chart.

###### Line chart mode

- The `Line` category encompasses all symbology parameters related to the styling of the line:
  - The `Color` parameter sets the HSV, RGB, or HTML tone values of the line

   ```{image} images/depth_viz_07_numerical_symbology_5.png
   :alt: depth_viz_07_numerical_symbology_5
   :class: bg-primary
   :width: 200px
   :align: center
   ```
  
  - The `Width` parameter sets the thickness of the line
  - The `Style` parameter sets the pattern of the line, the available options are: `CustomDashLine`, `DashDotDotLine`, `DashDotLine`, `DashLine`, `DotLine`, `NoPen`, and `SolidLine`
  
   ```{image} images/depth_viz_07_numerical_symbology_6.png
   :alt: depth_viz_07_numerical_symbology_6
   :class: bg-primary
   :width: 200px
   :align: center
   ```

  - The `Cap Style` parameter sets the shape of the ends of the segments as defined by the `Style` parameter, the available options are: `Flat`, `Round`, and `Square`
- The `Uncertainty` parameter toggles the display of error bars or box plots

  ```{image} images/depth_viz_07_numerical_symbology_1.png
   :alt: depth_viz_07_numerical_symbology_1
   :class: bg-primary
   :width: 300px
   :align: center
   ```

- The `Point` category encompasses all symbology parameters related to the styling of the data points:
  - The `Symbol` parameter sets the shape of the point, the available options are: `None`, `Circle`, `Square`, `Cross`, and `Dot`

   ```{image} images/depth_viz_07_numerical_symbology_7.png
   :alt: depth_viz_07_numerical_symbology_6
   :class: bg-primary
   :width: 200px
   :align: center
   ```

  - The `Size` parameter set the diameter of the points
  
   ```{image} images/depth_viz_07_numerical_symbology_3.png
   :alt: depth_viz_07_numerical_symbology_3
   :class: bg-primary
   :width: 300px
   :align: center
   ```

  - The `Color` parameter sets the HSV or RGB values of the points

   ```{image} images/depth_viz_07_numerical_symbology_5.png
   :alt: depth_viz_07_numerical_symbology_5
   :class: bg-primary
   :width: 200px
   :align: center
   ```

- The `Color ramp` category encompasses all symbology parameters related to value-dependent color styling of the line:
  - The `Name` entry provides a list of color ramps to choose from
  - The `Scale` entry displays the active color ramp and its markers
  - The `Min` parameter sets the lowest value to the mapped by the color ramp
  - The `Max` parameter sets the highest value to the mapped by the color ramp
  - The `Enable` parameter toggles the display of the color ramp

   ```{image} images/depth_viz_07_numerical_symbology_4.png
   :alt: depth_viz_07_numerical_symbology_4
   :class: bg-primary
   :width: 300px
   :align: center
   ```

##### Bar chart mode

- The `Bar pen` category encompasses all symbology parameters related to a bar chart elements
  - The `Color` parameter sets the HSV or RGB values of bar outlines

   ```{image} images/depth_viz_07_numerical_symbology_5.png
   :alt: depth_viz_07_numerical_symbology_5
   :class: bg-primary
   :width: 200px
   :align: center
   ```

  - The `Width` parameter sets the thickness of bar outlines
  - The `Style` parameter sets the pattern of bar outlines, the available options are: `CustomDashLine`, `DashDotDotLine`, `DashDotLine`, `DashLine`, `DotLine`, `NoPen`, and `SolidLine`
  - The `Cap Style` parameter sets the shape of the ends of bar outline segments as defined by the `Style` parameter, the available options are: `Flat`, `Round`, and `Square`
  - `Join Style`
- The `Bar fill` parameter sets the HSV or RGB values of bar surfaces

   ```{image} images/depth_viz_07_numerical_symbology_2.png
   :alt: depth_viz_07_numerical_symbology_2
   :class: bg-primary
   :width: 300px
   :align: center
   ```

#### Extended categorical data

In the case of extended numerical data, a series of vertically stacked and fully zoomed out interval logs will fill the right side of the visualization panel.

   ```{image} images/depth_viz_08_categorical_symbology.png
   :alt: depth_viz_08_categorical_symbology
   :class: bg-primary
   :width: 750px
   :align: center
   ```

- The `Bar pen` category encompasses all symbology parameters related to a categorical chart elements
  - The `Color` parameter sets the HSV or RGB values of bar outlines

   ```{image} images/depth_viz_07_numerical_symbology_5.png
   :alt: depth_viz_07_numerical_symbology_5
   :class: bg-primary
   :width: 200px
   :align: center
   ```

  - The `Width` parameter sets the thickness of bar outlines
  - The `Style` parameter sets the pattern of bar outlines, the available options are: `CustomDashLine`, `DashDotDotLine`, `DashDotLine`, `DashLine`, `DotLine`, `NoPen`, and `SolidLine`
  - The `Cap Style` parameter sets the shape of the ends of bar outline segments as defined by the `Style` parameter, the available options are: `Flat`, `Round`, and `Square`
  - the `Symbology` parameter provides access to the fill symbology table. Each line of the table maps a Category to the following parameters:

   ```{image} images/depth_viz_08_categorical_symbology_1.png
   :alt: depth_viz_08_categorical_symbology_1
   :class: bg-primary
   :width: 300px
   :align: center
   ```

    - The `Pattern` entries link to the SVG files that make up the pattern
    - The `Scale` entries define the relative size of the pattern
    - The `Color` entries set the HSV or RGB values of the background

   ```{image} images/depth_viz_07_numerical_symbology_5.png
   :alt: depth_viz_07_numerical_symbology_5
   :class: bg-primary
   :width: 200px
   :align: center
   ```

### Time domain downhole data visualization

> In progress

### Graph stacking

> In progress
