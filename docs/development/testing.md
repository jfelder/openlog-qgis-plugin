# Testing the plugin

Tests are written in 2 separate folders:

- `tests/unit`: testing code which is independent of QGIS API
- `tests/qgis`: testing code which depends on QGIS API

## Requirements

- 3.16 < QGIS < 3.99

```bash
python -m pip install -U -r requirements/testing.txt
```

## Run unit tests

```bash

pytest tests/qgis/gui
pytest tests/qgis/spatialite
pytest tests/qgis/xplordb --host=localhost --password=${PGPASSWORD} --db=${XPLORDB}
pytest tests/qgis/geotic --host=localhost --password=${MSSQL_SA_PASSWORD} --user=SA --db={GEOTICDB} --port 1433
```

```{Note}
You must run test for each database type with different steps, so you can define host/db/user/password/port for each database.
```